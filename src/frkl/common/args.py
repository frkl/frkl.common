# -*- coding: utf-8 -*-
import logging
from typing import Any, Dict, MutableMapping

log = logging.getLogger("frkl")


def parse_arg_type_string(
    arg_type_string: str, arg_type_key: str = "arg_type"
) -> MutableMapping[str, Any]:
    """Parse a string and create a argument prototing_config dict from it."""

    arg_type_string_new = arg_type_string
    arg_required = None
    arg_multiple = None

    if arg_type_string_new.endswith("!"):
        arg_type_string_new = arg_type_string_new[0:-1]
        arg_required = True
    elif arg_type_string_new.endswith("?"):
        arg_type_string_new = arg_type_string_new[0:-1]
        arg_required = False

    if arg_type_string_new.startswith("[") and arg_type_string_new.endswith("]"):
        arg_type_string_new = arg_type_string_new[1:-1]
        arg_multiple = True

        if arg_type_string_new[-1] in ["!", "?", "]"]:
            raise NotImplementedError(
                f"Handling of inner arg properties not implemented yet: {arg_type_string}"
            )

    result: Dict[str, Any] = {arg_type_key: arg_type_string_new}

    if arg_required is not None:
        result["required"] = arg_required
    if arg_multiple is not None:
        result["multiple"] = arg_multiple

    return result
