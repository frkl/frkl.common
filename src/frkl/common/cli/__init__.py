# -*- coding: utf-8 -*-
from rich.console import Console, ConsoleDimensions
from typing import Optional, Tuple

# Global console used by alternative print
_console: Optional[Console] = None


def get_console() -> Console:
    """Get a global Console instance.

    Returns:
        Console: A console instance.
    """
    global _console
    if _console is None:
        _console = Console()

    return _console


def get_terminal_size() -> Tuple:
    """Return the terminal size as a tuple (width, height)."""

    size: ConsoleDimensions = get_console().size
    return (size.width, size.height)


def output_to_terminal(line: str, nl: bool = True, no_output: bool = False):
    """Output a string to the terminal.

    This should work on most terminals.
    """

    if no_output:
        return

    if nl:
        end = "\n"
    else:
        end = ""
    get_console().print(line, end=end)
