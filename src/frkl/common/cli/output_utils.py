# -*- coding: utf-8 -*-
import tabulate
import textwrap
from rich import box
from rich.markdown import Markdown
from rich.style import Style
from rich.syntax import DEFAULT_THEME, Syntax
from rich.table import Table
from sortedcontainers import SortedDict
from typing import Any, Hashable, List, Mapping, Optional, Union

from frkl.common.cli import get_console
from frkl.common.formats.serialize import serialize, to_value_string


def to_key_value_table(
    data: Mapping[Hashable, Any],
    show_headers: bool = True,
    key_name: str = "Name",
    value_name: str = "Value",
    sort: bool = False,
) -> Table:

    table = Table(show_header=show_headers, box=box.SIMPLE)
    table.add_column(key_name, style="key2")
    table.add_column(value_name, style="value")

    if sort:
        data = SortedDict(data)

    for k, v in data.items():
        _k = to_value_string(k)
        _v = to_value_string(v)
        table.add_row(_k, _v)

    return table


def create_dict_block(
    _style: Union[str, Style] = "none", _code_theme: str = "monokai", **config: Any
) -> Markdown:

    config_yaml_string = serialize(config, format="yaml", indent=4, strip=True)
    config_markdown_string = f"``` yaml\n{config_yaml_string}\n```"
    config_markdown = Markdown(
        config_markdown_string, style=_style, code_theme=_code_theme, justify="left"
    )

    return config_markdown


def create_dict_element(
    _indent=0,
    _theme: str = DEFAULT_THEME,
    _prefix: Optional[str] = None,
    _postfix: Optional[str] = None,
    **config: Any,
) -> Syntax:

    config_yaml_string = serialize(config, format="yaml", indent=0, strip=True)
    if _prefix:
        config_yaml_string = _prefix + config_yaml_string
    if _postfix:
        config_yaml_string = config_yaml_string + _postfix

    block = Syntax(config_yaml_string, lexer_name="yaml", theme=_theme)

    return block


def create_two_column_table(
    data,
    header: Optional[List[str]] = None,
    tablefmt: str = "plain",
    alternate_row_colors=True,
) -> str:

    max_width = 0
    for f_name in data.keys():
        w = len(f_name)
        if w > max_width:
            max_width = w

    console = get_console()
    rest_width = console.width - max_width - 3
    max_rest = 20

    new_data = []
    toggle = True
    for key in data.keys():
        val = data[key]

        if rest_width > max_rest:
            val = "\n".join(textwrap.wrap(val, rest_width))

        # TODO: re-implement toggle
        # if toggle and alternate_row_colors:
        #     key = f"[italic]{key}[/italic]"
        #     val = f"[italic]{val}[/italic]"

        toggle = not toggle
        new_data.append([key, val])

    if rest_width > max_rest:

        _headers: Optional[List] = None
        if header:
            # TODO: make headers bold or something
            _headers = [header[0], header[1]]

            result = tabulate.tabulate(new_data, headers=_headers, tablefmt=tablefmt)
        else:
            result = tabulate.tabulate(new_data, tablefmt=tablefmt)

    else:
        result = ""
        for row in new_data:
            # TODO: make header bold
            if header:
                header_one = header[0]
                header_two = header[1]
                header_one_str = f"{header_one}"
                result = f"{result}{header_one_str}{row[0]}\n"
                result = f"{result}{header_two}{row[1]}\n\n"
            else:
                result = f"{result}{row[0]}\n"
                result = f"{result}{row[1]}\n\n"

    return result
