# -*- coding: utf-8 -*-
import asyncclick as click
import dpath
import json
import logging
import os
import typing
from asyncclick import BadParameter
from json.decoder import JSONDecodeError
from typing import Any, MutableMapping

from frkl.common.dicts import get_seeded_dict
from frkl.common.strings import is_url_or_abbrev

log = logging.getLogger("frkl")


class DictType(click.ParamType):
    def merge_multiple(
        self, *data: typing.Mapping[str, Any]
    ) -> MutableMapping[str, Any]:

        merged = get_seeded_dict(*data, merge_strategy="merge")
        return merged

    def __init__(self):

        super().__init__()

    name = "vars_type"

    def convert(self, value, param, ctx):

        if not isinstance(value, str):
            return value

        path = os.path.realpath(os.path.expanduser(value))
        value_type = "string"
        if os.path.exists(path):
            value_type = "local"
        elif is_url_or_abbrev(value):
            value_type = "remote"

        if value_type in ["remote", "local"]:

            raise Exception("Loading a dict from a file not implemented yet.")

        else:
            try:
                result = json.loads(value)
            except JSONDecodeError:
                if "=" in value:
                    k, v = value.split("=", maxsplit=1)

                    _parsed = json.loads(v)

                    result = {}
                    dpath.util.new(result, k, _parsed, separator=".")
                else:
                    raise BadParameter(
                        message=f"Can't parse string value '{value}' into dict",
                        param=param,
                    )

            return result
