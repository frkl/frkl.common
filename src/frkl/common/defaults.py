# -*- coding: utf-8 -*-
import os
import sys
from appdirs import AppDirs
from pathlib import Path

frkl_common_app_dirs = AppDirs("frkl_common", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_COMMON_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_common` module."""
else:
    FRKL_COMMON_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_common"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_common` module."""

FRKL_COMMON_RESOURCES_FOLDER = os.path.join(FRKL_COMMON_MODULE_BASE_FOLDER, "resources")


DEFAULT_URL_ABBREVIATIONS_FILE = {
    "gh": "https://raw.githubusercontent.com/{user}/{repo}/{branch}/{path}",
    "bb": "https://bitbucket.org/{user}/{repo}/src/{branch}/{path}",
    "gl": "https://gitlab.com/{user}/{repo}/raw/{branch}/{path}",
}
"""Built in url abbreviations for remote files."""

DEFAULT_URL_ABBREVIATIONS_GIT_REPO = {
    "gh": "https://github.com/{user}/{repo}.git",
    "bb": "https://bitbucket.org/{user}/{repo}.git",
    "gl": "https://gitlab.com/{user}/{repo}.git",
}
"""Built in url abbreviations for remote git repos."""

DEFAULT_EXCLUDE_DIRS = [".git", ".tox", ".cache"]
"""List of directory names to exclude by default when walking a folder recursively."""

DEFAULT_DOWNLOAD_CACHE_BASE = Path(os.path.expanduser("~/.cache/frkl/download_cache"))
"""Default cache location for downloads."""

DEFAULT_KEY_FORMATS = ["underscore"]
"""Default key formats for the Type-related helper methods/classes."""
FRKL_DEFAULT_SHARE_DIR = os.path.expanduser("~/.local/share/frkl")
FRKL_COLOR_PROGRESSION = [
    "blue",
    "dark_green",
    "bright_magenta",
    "grey66",
    "deep_pink4",
]

DEFAULT_LOG_NAMES = (
    "tings",
    "plugtings",
    "freckles",
    "ting",
    "frkl",
    "nsbl",
    "tempting",
    "shellting",
    "bring",
    "freckworks",
    "retailiate",
    "retailiate-client",
)
"""Default logger names to be used for batch-setting log-levels on application entry."""

FRKL_GIT_CHECKOUT_CACHE = os.path.join(
    frkl_common_app_dirs.user_cache_dir, "git_checkouts"
)

DEFAULT_IGNORE_MODULES = [
    "zipp",
    "yaspin",
    "yarl",
    "whichcraft",
    "wheel",
    "wcwidth",
    "watchgod",
    "watchdog",
    "vistir",
    "virtualenv",
    "validators",
    "uvloop",
    "urllib3",
    "typing-extensions",
    "typed-ast",
    "treelib",
    "traitlets",
    "tox",
    "tornado",
    "toml",
    "text-unidecode",
    "stevedore",
    "sortedcontainers",
    "sniffio",
    "smmap",
    "six",
    "setuptools",
    "setuptools-scm",
    "scp",
    "scalpl",
    "s3transfer",
    "ruamel.yaml",
    "ruamel.yaml.clib",
    "rfc3986",
    "requests",
    "regex",
    "pyyaml",
    "pyupdater",
    "pyupdater-scp-plugin",
    "pyupdater-s3-plugin",
    "pytz",
    "python-slugify",
    "python-dateutil",
    "pytest",
    "pytest-cov",
    "pypubsub",
    "pyparsing",
    "pynacl",
    "pymdown-extensions",
    "pyinstaller",
    "pygments",
    "pyflakes",
    "pydriller",
    "pydantic",
    "pycparser",
    "pycodestyle",
    "py",
    "ptyprocess",
    "prompt-toolkit",
    "pp-ez",
    "poyo",
    "portray",
    "pluggy",
    "pkg-resources",
    "pipdeptree",
    "pip",
    "pip-tools",
    "pickleshare",
    "pexpect",
    "pdocs",
    "pbr",
    "pathtools",
    "pathspec",
    "parso",
    "paramiko",
    "packaging",
    "nuitka",
    "nodeenv",
    "nltk",
    "mypy",
    "mypy-extensions",
    "multidict",
    "more-itertools",
    "mkdocs",
    "mkdocs-material",
    "mccabe",
    "markupsafe",
    "markdown",
    "mako",
    "lunr",
    "logzero",
    "lizard",
    "livereload",
    "kubernetes-asyncio",
    "jmespath",
    "jinja2",
    "jinja2-time",
    "jedi",
    "isort",
    "ipython",
    "ipython-genutils",
    "importlib-resources",
    "importlib-metadata",
    "immutables",
    "idna",
    "idna-ssl",
    "identify",
    "hyperframe",
    "hug",
    "httpx",
    "hstspreload",
    "hpack",
    "h11",
    "h2",
    "gitpython",
    "gitdb",
    "future",
    "formic2",
    "flake8",
    "filelock",
    "fastavro",
    "falcon",
    "examples",
    "entrypoints",
    "ed25519",
    "dsdev-utils",
    "dpath",
    "docutils",
    "distlib",
    "dictdiffer",
    "decorator",
    "dataclasses",
    "cryptography",
    "cruft",
    "coverage",
    "cookiecutter",
    "contextvars",
    "colorama",
    "click",
    "click-aliases",
    "chardet",
    "cfgv",
    "cffi",
    "certifi",
    "pre-commit",
    "bsdiff4",
    "botocore",
    "boto3",
    "blessed",
    "black",
    "binaryornot",
    "bcrypt",
    "backcall",
    "attrs",
    "async-timeout",
    "async-generator",
    "async-exit-stack",
    "asgiref",
    "arrow",
    "appdirs",
    "anyio",
    "altgraph",
    "aiohttp",
    "asyncclick",
]
"""Modules to ignore when looking up (relevant) package dependencies."""
