# -*- coding: utf-8 -*-
import collections
import copy
import typing
from collections.abc import Mapping

from frkl.common.exceptions import FrklException


def dict_merge(
    dct: typing.Mapping, merge_dct: typing.Mapping, copy_dct: bool = True
) -> typing.MutableMapping:
    """Recursive dict merge. Inspired by :meth:``dict.update()``.

    Instead of updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into ``dct``.

    Copied from: https://gist.github.com/angstwad/bf22d1822c38a92ec0a9

    Args:
      - *dct*: dict onto which the merge is executed
      - *merge_dct*: dct merged into dct
      - *copy_dct*: whether to (deep-)copy dct before merging (and leaving it unchanged), or not (default: copy)

    Returns:
      - the merged dict (original or copied)
    """

    if copy_dct:
        if isinstance(dct, collections.abc.MutableMapping):
            _dct: typing.MutableMapping = copy.deepcopy(dct)
        else:
            _dct = copy.deepcopy(dict(dct))
    elif not isinstance(dct, collections.abc.MutableMapping):
        _dct = copy.deepcopy(dict(dct))
    else:
        _dct = dct

    for k, v in merge_dct.items():
        if (
            k in _dct
            and isinstance(_dct[k], Mapping)
            and isinstance(merge_dct[k], Mapping)
        ):
            dict_merge(_dct[k], merge_dct[k], copy_dct=False)
        else:
            _dct[k] = merge_dct[k]

    return _dct


def merge_list_of_dicts(
    dicts: typing.List[typing.MutableMapping],
    starting_dict: typing.MutableMapping = None,
):
    """Merges a list of dicts.

    Args:
      - *dicts*: list of dicts to be merged in order
      - *starting_dict*: (optional) existing dict where the others are merged into

    Returns:
      - the merged dict (same as starting_dict)
    """

    if starting_dict is None:
        starting_dict = {}
    for d in dicts:
        dict_merge(starting_dict, d, copy_dct=False)

    return starting_dict


def get_seeded_dict(
    *seed_dicts: typing.Optional[typing.Mapping],
    merge_strategy: str = "update",
    deep_copy: bool = False,
) -> typing.MutableMapping:

    result: typing.Dict = {}

    for seed in seed_dicts:
        if not seed:
            continue

        if deep_copy:
            seed = copy.deepcopy(seed)
        if merge_strategy == "update":
            result.update(seed)
        elif merge_strategy == "merge":
            dict_merge(result, seed, copy_dct=False)
        else:
            raise FrklException(
                msg="Can't create seeded dict.",
                reason=f"Invalid merge strategy '{merge_strategy}'. Use one of: 'update', 'merge'.",
            )

    return result
