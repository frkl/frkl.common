# -*- coding: utf-8 -*-
import collections.abc
import inspect
import logging
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap
from typing import (
    TYPE_CHECKING,
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Tuple,
    Type,
    Union,
)

if TYPE_CHECKING:
    from rich.console import Console, ConsoleOptions, RenderResult
    from rich.table import Table


log = logging.getLogger("frutils")


def filter_metadata(
    help_string: Optional[str],
    metadata_keys: Optional[Iterable[str]] = None,
    ignore_case: bool = True,
) -> Tuple[Optional[str], MutableMapping[str, Any]]:

    if not metadata_keys or not help_string:
        return (help_string, {})

    metadata: Dict[str, List[str]] = {}
    filtered_help = []
    current_key = None
    lines = None
    for line in help_string.split("\n"):

        if current_key is None and not line.strip():
            filtered_help.append(line)
            continue

        if current_key and (not line.startswith(" ") and not line.startswith("\t ")):
            metadata[current_key] = lines
            current_key = None
            lines = None

        if current_key is None:
            matched = False
            for key in metadata_keys:
                if ignore_case:
                    match = line.strip().lower()
                    match_2 = key.lower()
                else:
                    match = line.strip()
                    match_2 = key

                if match == f"{match_2}:":
                    current_key = key.lower()
                    lines = []
                    matched = True
                    break

            if matched:
                continue

        if current_key is None:
            filtered_help.append(line)
        else:
            lines.append(line)

    if current_key is not None:
        metadata[current_key] = lines

    yaml_string = ""
    for k, v in metadata.items():
        if not v:
            continue
        yaml_string += f"{k}:\n"
        for line in v:
            yaml_string += line + "\n"

    if not yaml_string:
        metadata_dict = {}
    else:
        yaml = YAML(typ="safe")
        metadata_dict = yaml.load(yaml_string)

    new_help = "\n".join(filtered_help)

    return (new_help.strip(), metadata_dict)


class Doc(object):
    @classmethod
    def to_list_item_format(self, msg, first_char_lowercase=True):

        if first_char_lowercase:
            msg = msg[0].lower() + msg[1:]

        if msg.endswith("."):
            msg = msg[0:-1]

        if msg.startswith("[") and msg.endswith("]"):
            msg = msg[1:-1]

        return msg

    @classmethod
    def from_docstring(
        cls,
        func_or_class: Union[Callable, Type],
        metadata_keys: Optional[Iterable[str]] = None,
    ):

        doc = func_or_class.__doc__
        if not doc:
            try:
                doc = func_or_class.__init__.__doc__  # type: ignore
            except Exception:
                pass

        if doc is None:
            return Doc()
        doc = inspect.cleandoc(doc)

        doc_dict = Doc.doc_dict_from_string(doc, metadata_keys=metadata_keys)
        return Doc(doc_dict)

    @classmethod
    def doc_dict_from_string(
        self, doc_string: str, metadata_keys: Optional[Iterable[str]] = None
    ) -> MutableMapping[str, Any]:

        doc_string = doc_string.strip()
        if "\n" in doc_string:
            short_help = doc_string.split("\n")[0]
            _help = doc_string.split("\n")[1:]
        else:
            short_help = doc_string
            _help = []

        help: Optional[str] = "\n".join(_help)

        metadata_dict: MutableMapping[str, Any]
        help, metadata_dict = filter_metadata(
            help_string=help, metadata_keys=metadata_keys
        )

        if help is not None:
            help = help.strip()

        metadata_dict["short_help"] = short_help.strip()
        metadata_dict["help"] = help

        return metadata_dict

    @classmethod
    def __get_validators__(cls):
        # one or more validators may be yielded which will be called in the
        # order to validate the input, each validator will receive as an input
        # the value returned from the previous validator
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        # field_schema.update(
        #     # simplified regex here for brevity, see the wikipedia link above
        #     pattern='^[A-Z]{1,2}[0-9][A-Z0-9]? ?[0-9][A-Z]{2}$',
        #     # some example postcodes
        #     examples=['SP11 9DG', 'w1j7bu'],
        # )
        pass

    @classmethod
    def validate(cls, v):

        if not v:
            return Doc()
        elif isinstance(v, Doc):
            return v
        elif isinstance(v, (str, Mapping)):
            return Doc(v)
        else:
            raise ValueError(f"Can't create doc object, invalid type '{type(v)}'.")

    def __init__(
        self,
        data: Union[str, Mapping[str, Any], None] = None,
        short_help_key: Optional[str] = "short_help",
        help_key: Optional[str] = "help",
        metadata_key: Optional[str] = None,
    ):

        if short_help_key is None:
            short_help_key = "short_help"
        if help_key is None:
            help_key = "help"

        if not data:
            _data: MutableMapping[str, Any] = {}
        elif isinstance(data, str):
            _data = Doc.doc_dict_from_string(data)
        elif isinstance(data, collections.abc.Mapping):
            _data = dict(data)
        else:
            raise TypeError(
                "Doc needs a string or mapping for initialization, not '{}'".format(
                    type(data)
                )
            )

        self._short_help: Optional[str] = _data.pop(short_help_key, None)
        self._help: Optional[str] = _data.pop(help_key, None)
        if metadata_key is None:
            md = _data
        else:
            md = _data.get(metadata_key, None)

        if md is None:
            md = {}
        else:
            md = dict(md)

        self._metadata: MutableMapping[str, Any] = md

    @property
    def metadata(self) -> Mapping[str, Any]:

        return self._metadata

    def extract_metadata(self, *metadata_keys: str) -> None:
        """Parse 'help' attribute for yaml-formatted metadata.

        This will change the value for 'help' by filtering the matching metadata regions.
        """

        if not metadata_keys:
            return

        missing: List[str] = []
        for key in metadata_keys:

            if key in self._metadata.keys():
                log.debug(f"Metadata key '{key}' already present, doing nothing...")
                continue

            missing.append(key)

        help, metadata = filter_metadata(self._help, metadata_keys=missing)

        self._help = help
        self._metadata.update(metadata)

    def get_short_help(self, default="n/a", list_item_format=False):

        result = self._short_help

        if not result:
            return default

        if list_item_format:
            result = Doc.to_list_item_format(result)

        result = result.strip()
        return result

    def get_help(self, default: Optional[str] = None, use_short_help: bool = False):

        result = self._help
        if not result:
            if self._short_help and use_short_help:
                result = self.get_short_help(default=default)
            else:
                result = default

        else:
            if use_short_help and self._short_help:
                if not result.startswith(self._short_help):
                    result = self._short_help + "\n\n" + result

        if result:
            result = result.strip()

        return result

    def matches_apropos(self, apropos, only_short_help=True):
        """Checks whether this documentations text matches all of the apropos matchers."""

        if not apropos:
            return False

        if isinstance(apropos, str):
            apropos = [apropos]

        match = True
        for a in apropos:
            if only_short_help:
                if a.lower() not in self.get_short_help().lower():
                    match = False
                    break

            else:
                if (a.lower() not in self.get_short_help().lower()) and (
                    a.lower() not in self.get_help().lower()
                ):
                    match = False
                    break
        return match

    def get_metadata_value(self, key: str, default=None) -> Any:

        return self._metadata.get(key, default)

    def exploded_dict(self, default_not_available_string="n/a") -> CommentedMap:

        result = CommentedMap()
        if self._short_help:
            result["short_help"] = self.get_short_help(
                default=default_not_available_string, list_item_format=False
            )
        else:
            result["short_help"] = default_not_available_string
        if self._help:
            result["help"] = self.get_help(default=default_not_available_string)
        else:
            result["help"] = default_not_available_string
        result["metadata"] = self._metadata

        return result

    def to_dict(self):

        return self.exploded_dict()

    @classmethod
    def from_json(cls, data):

        if not isinstance(data, collections.abc.Mapping):
            raise TypeError(
                "Invalid data type '{}', needs a mapping.".format(type(data))
            )
        return Doc(**data)

    def __str__(self):

        if self._short_help and self._help:
            result = (
                f"{self.get_short_help(list_item_format=False)}\n\n{self.get_help()}"
            )
            return result
        elif self._short_help and not self._help:
            result = self._short_help
            return result
        elif not self._short_help and self._help:
            return self._help
        else:
            return "no data"

    def __repr__(self):

        from frkl.common.formats.serialize import serialize

        result = serialize(self.exploded_dict(), format="yaml")
        return result

    def __rich_console__(
        self, console: "Console", options: "ConsoleOptions"
    ) -> "RenderResult":

        if self.get_help(default=None):
            result_string = self.get_help()
        else:
            result_string = ""

        yield result_string

        if self._metadata:
            yield ""
            yield "[bold]Metadata[/bold]"
            table = create_table_from_dict(self._metadata)

            yield table


def create_table_from_dict(data: Mapping[str, Any]) -> "Table":

    from rich import box
    from rich.table import Table

    table = Table(box=box.SIMPLE, show_header=False)
    table.add_column("Name")
    table.add_column("Value", style="italic")
    for k, v in sorted(data.items()):

        if v is not False and not v:
            continue

        if k == "metadata_timestamp":
            try:
                import arrow

                v = arrow.get(v).humanize()
            except Exception:
                v = str(v)

        if isinstance(v, str):
            table.add_row(k, v)
        elif isinstance(v, (collections.abc.Mapping, collections.abc.Iterable)):
            from frkl.common.formats.serialize import serialize

            v = serialize(v, format="yaml", strip=True)

            lines = v.split("\n")
            table.add_row(k, "")
            for line in lines:
                table.add_row("", line)
        else:
            table.add_row(k, str(v))

    return table
