# -*- coding: utf-8 -*-
import json
import os
from enum import Enum
from pathlib import Path
from typing import Optional, Union

from frkl.common.exceptions import FrklException
from frkl.common.filesystem import ensure_folder


class FILE_CONTENT_TYPE(Enum):

    unknown = 0
    bytes = 1
    text = 2


async def download_file_async(
    url: str,
    target_file: Optional[Union[str, Path]] = None,
    return_content: bool = True,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
    store_file_metadata: bool = False,
) -> Union[bytes, str]:
    """Download a file asynchronously.

    If no 'file_type' is provided (or set to REMOTE_FILE_TYPE.unknown), this function will try to parse the result content as text, if that fails, bytes will be returned/written.

    Args:
        url (str): the url to download
        target_file (None, str, Path): the path to write the file content to
        return_content (bool): if set to true, return the content of the downloaded file, otherwise the path to the target_file (if provided)
        content_type (FILE_CONTENT_TYPE): the expected content type of the remote file
        store_file_metadtaa (bool): whether to store file type (binary/text) and encoding (if text) to a file ending with '.metadata.json'

    Returns:
        (bytes, str): the content or path to the downloaded file
    """

    if not target_file and not return_content:
        raise ValueError(
            "Can't download file: no 'target_file' provided, and 'return_content' set to False."
        )

    if content_type is None:
        content_type = FILE_CONTENT_TYPE.unknown

    import httpx

    async with httpx.AsyncClient() as client:
        r = await client.get(url)

    r.raise_for_status()

    if content_type == FILE_CONTENT_TYPE.text:
        result: Union[str, bytes] = r.text
        actual_type = FILE_CONTENT_TYPE.text
    elif content_type == FILE_CONTENT_TYPE.bytes:
        result = r.content
        actual_type = FILE_CONTENT_TYPE.bytes
    else:

        if not r.content:
            result = ""
            actual_type = FILE_CONTENT_TYPE.text
        else:
            if r.headers["content-type"].startswith("text/"):
                result = r.text
                actual_type = FILE_CONTENT_TYPE.text
            else:
                result = r.content
                actual_type = FILE_CONTENT_TYPE.bytes

    if target_file:

        if isinstance(target_file, Path):
            target_file = os.path.abspath(target_file.as_posix())
        else:
            target_file = os.path.abspath(os.path.expanduser(target_file))

        ensure_folder(os.path.dirname(target_file))

        try:
            from anyio import open_file

            if actual_type == FILE_CONTENT_TYPE.bytes:
                async with await open_file(target_file, "wb") as f:
                    await f.write(result)
            else:
                async with await open_file(target_file, "w") as f:
                    await f.write(result)

            if store_file_metadata:
                md_path = f"{target_file}.metadata.json"
                metadata = {
                    "type": actual_type.name,
                    "content-type": r.headers.get("content-type", ""),
                }
                enc = r.encoding
                if enc:
                    metadata["encoding"] = enc

                async with await open_file(md_path, "w") as f_md:
                    await f_md.write(json.dumps(metadata))

        except (Exception) as e:
            raise FrklException(
                msg=f"Could not save content from url '{url}' to: {target_file}",
                parent=e,
            )

        if return_content:
            return result
        else:
            return target_file
    else:
        return result


def download_file(
    url: str,
    target_file: Optional[Union[str, Path]] = None,
    return_content: bool = True,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
    store_file_metadata: bool = False,
) -> Union[bytes, str]:
    """Download a file synchronously.

    If no 'file_type' is provided (or set to REMOTE_FILE_TYPE.unknown), this function will try to parse the result content as text, if that fails, bytes will be returned/written.

    Args:
        url (str): the url to download
        target_file (None, str, Path): the path to write the file content to
        return_content (bool): if set to true, return the content of the downloaded file, otherwise the path to the target_file (if provided)
        content_type (FILE_CONTENT_TYPE): the expected content type of the remote file
        store_file_metadtaa (bool): whether to store file type (binary/text) and encoding (if text) to a file ending with '.metadata.json'

    Returns:
        (bytes, str): the content or path to the downloaded file
    """

    if not target_file and not return_content:
        raise ValueError(
            "Can't download file: no 'target_file' provided, and 'return_content' set to False."
        )

    if content_type is None:
        content_type = FILE_CONTENT_TYPE.unknown

    import httpx

    r = httpx.get(url, verify=True)
    r.raise_for_status()

    if content_type == FILE_CONTENT_TYPE.text:
        result: Union[str, bytes] = r.text
        actual_type = FILE_CONTENT_TYPE.text
    elif content_type == FILE_CONTENT_TYPE.bytes:
        result = r.content
        actual_type = FILE_CONTENT_TYPE.bytes
    else:
        content = r.content
        if not content:
            result = ""
            actual_type = FILE_CONTENT_TYPE.text
        else:
            if not r.content:
                result = ""
                actual_type = FILE_CONTENT_TYPE.text
            else:
                if r.headers["content-type"].startswith("text/"):
                    result = r.text
                    actual_type = FILE_CONTENT_TYPE.text
                else:
                    result = r.content
                    actual_type = FILE_CONTENT_TYPE.bytes

    if target_file:
        if isinstance(target_file, Path):
            _target_file: str = os.path.abspath(target_file.as_posix())
        else:
            _target_file = os.path.abspath(os.path.expanduser(target_file))

        ensure_folder(os.path.dirname(_target_file))
        try:
            if actual_type == FILE_CONTENT_TYPE.bytes:
                if not isinstance(result, bytes):
                    raise TypeError(
                        f"Can't write bytes to file '{_target_file}': invalid type '{type(result)}'"
                    )
                with open(_target_file, "wb") as f_b:
                    f_b.write(result)
            else:
                if not isinstance(result, str):
                    raise TypeError(
                        f"Can't write text to file '{_target_file}': invalid type '{type(result)}'"
                    )
                with open(_target_file, "w") as f_t:
                    f_t.write(result)

            if store_file_metadata:
                md_path = f"{_target_file}.metadata.json"
                metadata = {
                    "type": actual_type.name,
                    "content-type": r.headers.get("content-type", ""),
                }
                enc = r.encoding
                if enc:
                    metadata["encoding"] = enc

                with open(md_path, "w") as f_md:
                    json.dump(metadata, f_md)

        except (Exception) as e:
            raise FrklException(
                msg=f"Could not save content from url '{url}' to: {_target_file}",
                parent=e,
            )

        if return_content:
            return result
        else:
            return _target_file
    else:
        return result
