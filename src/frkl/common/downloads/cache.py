# -*- coding: utf-8 -*-
import json
import os
import re
from anyio import open_file
from pathlib import Path
from typing import Optional, Union

from frkl.common.defaults import DEFAULT_DOWNLOAD_CACHE_BASE
from frkl.common.downloads import FILE_CONTENT_TYPE, download_file, download_file_async


def calculate_cache_location_for_url(
    url: str,
    postfix: Optional[str] = None,
    repl_chars: Optional[str] = None,
    sep: str = os.path.sep,
) -> str:
    """Utility method to get a unique path that can be used for caching a download.

    Args:
        url (str): the url to download/cache
        postfix (Optional[str]): an optional postfix to add to the url before calculating the cache location
        repl_chars (Optional[str]): the chars to replace with a path seperator when calculating the location (you probably don't need this)
        sep (str): the separator string, defaults to OS-specific path seperator

    Returns:
        str: a relative (valid) path-name
    """

    if repl_chars is None:
        repl_chars = "[^_\\-A-Za-z0-9.]+"
    if postfix is not None:
        temp = os.path.join(url, postfix)
    else:
        temp = url

    if os.name != "nt":
        repl = sep
    else:
        repl = "___X___"

    path = re.sub(repl_chars, repl, temp)
    if os.name == "nt":
        path = path.replace("___X___", sep)

    return path


def calculate_cache_path(
    base_path: Union[str, Path], url: str, postfix: str = None
) -> str:
    """Calculate the (absolute) unique path for a cache location for the specified url.

     Args:
         base_path (Union[str, Path]): the base path of your cache
        url (str): the url to download/cache
        postfix (Optional[str]): an optional postfix to add to the url before calculating the cache location
        repl_chars (Optional[str]): the chars to replace with a path
    Returns:
        str: the absolute path to the cache location for the specified url
    """

    if not isinstance(base_path, str):
        base_path = os.path.abspath(base_path.resolve().as_posix())
    else:
        base_path = os.path.abspath(base_path)

    cache_path = calculate_cache_location_for_url(url=url, postfix=postfix)

    result = os.path.join(base_path, cache_path)
    return result


def _get_cached_file_path(url: str, cache_base: Union[str, Path] = None):
    """Calculate the cache path for the url/cache_base combination."""

    if not isinstance(url, str):
        raise Exception("Url needs to be string")

    if cache_base is None:
        cache_base = DEFAULT_DOWNLOAD_CACHE_BASE

    rel_cache_path = calculate_cache_location_for_url(url)
    if isinstance(cache_base, Path):
        cache_base = os.path.abspath(cache_base.as_posix())
    else:
        cache_base = os.path.abspath(os.path.expanduser(cache_base))

    cached_file = os.path.join(cache_base, rel_cache_path)
    return cached_file


def get_cached_file(
    url: str,
    cache_base: Optional[Union[str, Path]] = None,
    return_content: bool = False,
    update: bool = False,
    file_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes, None]:
    """Return the cached file (or it's content), or None if no cache exists."""

    cached_file = _get_cached_file_path(url=url, cache_base=cache_base)
    result = _get_cached_file(
        cached_file,
        return_content=return_content,
        update=update,
        content_type=file_type,
    )
    return result


def _get_cached_file(
    cached_file: str,
    return_content: bool = False,
    update: bool = False,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes, None]:

    if update:
        return None

    if content_type is None:
        content_type = FILE_CONTENT_TYPE.unknown

    if not os.path.exists(cached_file):
        return None

    if os.path.isdir(os.path.realpath(cached_file)):
        raise Exception("Target cache file is not a file: {}".format(cached_file))

    if not return_content:
        return cached_file

    if content_type == FILE_CONTENT_TYPE.unknown:
        metadata_file = f"{cached_file}.metadata.json"
        with open(metadata_file) as f_md:
            metadata = json.load(f_md)

        content_type = FILE_CONTENT_TYPE[metadata["type"]]

    if content_type == FILE_CONTENT_TYPE.text:

        with open(cached_file) as f_b:
            content: Union[str, bytes] = f_b.read()

    elif content_type == FILE_CONTENT_TYPE.bytes:
        with open(cached_file, "b") as f_t:
            content = f_t.read()

    return content


async def get_cached_file_async(
    url: str,
    cache_base: Optional[Union[str, Path]] = None,
    return_content: bool = False,
    update: bool = False,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes, None]:
    """Return the cached file (or it's content), or None if no cache exists."""

    cached_file = _get_cached_file_path(url=url, cache_base=cache_base)
    result = await _get_cached_file_async(
        cached_file,
        return_content=return_content,
        update=update,
        content_type=content_type,
    )
    return result


async def _get_cached_file_async(
    cached_file: str,
    return_content: bool = False,
    update: bool = False,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes, None]:

    if update:
        return None

    if content_type is None:
        content_type = FILE_CONTENT_TYPE.unknown

    if not os.path.exists(cached_file):
        return None

    if os.path.isdir(os.path.realpath(cached_file)):
        raise Exception("Target cache file is not a file: {}".format(cached_file))

    if not return_content:
        return cached_file

    if content_type == FILE_CONTENT_TYPE.unknown:

        metadata_file = f"{cached_file}.metadata.json"
        async with await open_file(metadata_file) as f_md:
            content: Union[str, bytes] = await f_md.read()
            metadata = json.loads(content)

        content_type = FILE_CONTENT_TYPE[metadata["type"]]

    if content_type == FILE_CONTENT_TYPE.text:
        async with await open_file(cached_file) as f:
            content = await f.read()

    elif content_type == FILE_CONTENT_TYPE.bytes:
        async with await open_file(cached_file, "rb") as f:
            content = await f.read()

    return content


def download_cached_file(
    url: str,
    update: bool = False,
    cache_base: Optional[Union[str, Path]] = None,
    return_content: bool = False,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes]:
    """Downloads a file using a full or abbreviated url.

    Args:
      url: the url
      update: whether to 'force' update the file if it doesn't exist
      cache_base: root of cache directory
      return_content: whether to return the path to the file (False) or it's content (True)
      content_type (Optional[FILE_CONTENT_TYPE]): whether the remote file is supposed to be text or binary

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    cached_file_path = _get_cached_file_path(url=url, cache_base=cache_base)

    cached_file = _get_cached_file(
        cached_file=cached_file_path,
        return_content=return_content,
        update=update,
        content_type=content_type,
    )

    if cached_file is not None:
        return cached_file

    downloaded_file = download_file(
        url=url,
        target_file=cached_file_path,
        return_content=return_content,
        content_type=content_type,
        store_file_metadata=True,
    )

    return downloaded_file


async def download_cached_file_async(
    url: str,
    update: bool = False,
    cache_base: Optional[Union[str, Path]] = None,
    return_content: bool = False,
    content_type: Optional[FILE_CONTENT_TYPE] = None,
) -> Union[str, bytes]:
    """Downloads a file using a full or abbreviated url.

    Args:
      url: the url
      update: whether to 'force' update the file if file exists in cache
      cache_base: root of cache directory
      return_content: whether to return the path to the file (False) or it's content (True)
      content_type (Optional[FILE_CONTENT_TYPE]): whether the remote file is supposed to be text or binary

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    cached_file_path = _get_cached_file_path(url=url, cache_base=cache_base)

    cached_file = await _get_cached_file_async(
        cached_file=cached_file_path,
        return_content=return_content,
        update=update,
        content_type=content_type,
    )

    if cached_file is not None:
        return cached_file

    downloaded_file = await download_file_async(
        url=url,
        target_file=cached_file_path,
        return_content=return_content,
        content_type=content_type,
        store_file_metadata=True,
    )

    return downloaded_file
