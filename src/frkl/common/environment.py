# -*- coding: utf-8 -*-
import os
from typing import Iterable, Optional, Union

from frkl.common.iterables import ensure_iterable


def get_env_var(name: str) -> Optional[str]:

    value = os.getenv(name)
    if value:
        return value

    if name.islower():
        value = os.getenv(name.upper())
        if value:
            return value

    return None


def get_var_value_from_env(
    *names: str,
    prefixes: Optional[Union[str, Iterable[str]]] = None,
    only_check_prefixes: bool = False,
) -> Optional[str]:
    """Checks whether an environemt variable exists for one of the provided names.

    If prefixes are specified, also check whether <prefix><name> exist.

    If a name is all lowercase, environment variables will be checked that match an all-lowercase or all-uppercase environment variable.
    If a name is mixed or all uppercase, an exact match is needed.

    First non-empty match will be returned, or None.
    """

    if not only_check_prefixes:
        for name in names:
            value = get_env_var(name)
            if value:
                return value

    if not prefixes:
        return None

    for prefix in ensure_iterable(prefixes):  # type: ignore
        for name in names:
            full_name = f"{prefix}{name}"
            value = get_env_var(full_name)
            if value:
                return value

    return None


def is_debug() -> bool:
    """Return whether the environment variable ``DEBUG`` is set to ``true`` or ``yes`` (case-independent)."""

    debug = os.environ.get("DEBUG", "false")
    return debug.lower() in ["true", "yes"]
