# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping, Optional

from frkl.common.formats import CONTENT_TYPE


def is_frkl_exception(exc: Any):

    return isinstance(exc, FrklException) or issubclass(exc.__class__, FrklException)


class FrklException(Exception):
    """Base exception class with a few helper methods to make it easy to display a good error message.

    Args:
      msg: the message to display
      solution: a recommendation to the user to fix or debug the issue
      references: a dict with references to the issue, title/url key-value pairs.
      reason: the reason for the exception
      parent: a parent exception
    """

    def __init__(
        self,
        msg: Optional[str] = None,
        solution: Optional[str] = None,
        references: Optional[Mapping] = None,
        reason: Optional[str] = None,
        parent: Optional[Exception] = None,
        **kwargs: Any,
    ):

        if isinstance(msg, Exception) or issubclass(msg.__class__, Exception):
            if parent is None:
                parent = msg

        super(FrklException, self).__init__(msg)

        self._msg: Optional[str] = msg
        self.solution: Optional[str] = solution
        self._reason: Optional[str] = reason
        self.references = references
        self.parent: Optional[Exception] = parent
        self._additional_args = kwargs

    @property
    def msg(self):

        if self._msg is not None:
            return self._msg
        if self.parent is not None:
            return str(self.parent)

        return "Unspecified error."

    @property
    def reason(self) -> Optional[str]:

        if not self._reason:
            return None

        if isinstance(self._reason, str):
            return self._reason
        else:
            return str(self._reason)

    @property
    def message(self):

        msg = self.msg
        if not msg.endswith("."):
            msg = msg + "."

        msg = msg + "\n"

        if self.reason:
            msg = msg + "\n  Reason: {}".format(self.reason)
        elif self.parent:
            if isinstance(self.parent, FrklException) or issubclass(
                self.parent.__class__, FrklException
            ):
                msg_parent = self.parent.msg
            else:
                msg_parent = str(self.parent)

            msg = msg + f"\n  Parent error: {msg_parent}"
        if self.solution:
            msg = msg + "\n  Solution: {}".format(self.solution)
        if self.references:
            if len(self.references) == 1:
                url = self.references[list(self.references.keys())[0]]
                msg = msg + "\n  Reference: {}".format(url)
            else:
                msg = msg + "\n  References\n"
                for k, v in self.references.items():
                    msg = msg + "\n    {}: {}".format(k, v)

        return msg.rstrip()

    @property
    def message_short(self):

        msg = self.msg
        if not msg.endswith("."):
            msg = msg + "."

        if self.reason:
            reason = self.reason
            if not reason.endswith("."):
                reason = reason + "."
            msg = f"{msg} {reason}"
        elif self.parent:
            if isinstance(self.parent, FrklException) or issubclass(
                self.parent.__class__, FrklException
            ):
                reason = self.parent.msg
            else:
                reason = str(self.parent)

            reason = reason.strip()
            if not reason.endswith("."):
                reason = reason + "."
            if not msg == reason:
                msg = f"{msg} {reason}"
        if self.solution:
            solution = self.solution
            if not solution.endswith("."):
                solution = solution + "."
            msg = f"{msg} {solution}"

        msg = msg.replace("\n", " ")

        return msg

    def to_dict(self) -> Mapping[str, Any]:

        result = {}
        result["msg"] = self.msg
        result["reason"] = self.reason
        result["parent"] = self.parent
        result["solution"] = self.solution
        result["references"] = self.references

        return result

    def __str__(self):

        return self.message_short

    def __repr__(self):
        return self.message


class FrklExtendedException(FrklException):
    """Exception that slurps up parent exception information, but allows for additional attriburtes."""

    def __init__(self, **kwargs):

        parent = kwargs.get("parent", None)
        if not is_frkl_exception(parent):
            new_kwargs = kwargs
        else:
            new_kwargs = parent.to_dict()  # type: ignore
            new_kwargs.update(kwargs)

        super().__init__(**new_kwargs)


class FrklMultiParentException(FrklException):
    """Exception that wraps several other exceptions."""

    def __init__(self, *parents: Exception, **kwargs):

        self._parents: Iterable[Exception] = parents
        if not parents:

            super().__init__(**kwargs)

        elif len(parents) == 1:
            if is_frkl_exception(parents[0]):
                new_kwargs = parents[0].to_dict()  # type: ignore
            else:
                new_kwargs = kwargs

            super().__init__(parent=parents[0], **new_kwargs)

        else:
            if "msg" not in kwargs.keys():
                kwargs["msg"] = "Multiple errors."

            if "reason" not in kwargs.keys():
                reasons = []

                for e in parents:
                    reasons.append(f"- {e}")
                kwargs["reason"] = "\n".join(reasons)

        super().__init__(**kwargs)


class FrklParseException(FrklException):
    """Exception to hold information about a parsing error.

    Args:
      content: the content that failed to be parsed
      msg: the message to display
      solution: a recommendation to the user to fix or debug the issue
      references: a dict with references to the issue, title/url key-value pairs.
      reason: the reason for the exception
      parent: a parent exception
      content_origin: metadata, a reference where the content came from
    """

    def __init__(
        self,
        content: str,
        msg: Optional[str] = None,
        solution: Optional[str] = None,
        references: Optional[Mapping] = None,
        reason: Optional[str] = None,
        exception_map: Optional[Mapping[CONTENT_TYPE, Exception]] = None,
    ):
        self._content = content

        if exception_map is None:
            exception_map = {}
        self._exception_map: Mapping[CONTENT_TYPE, Exception] = exception_map
        super(FrklParseException, self).__init__(
            msg=msg, reason=reason, solution=solution, references=references
        )

    @property
    def content(self) -> str:
        return self._content

    @property
    def exception_map(self) -> Mapping[CONTENT_TYPE, Exception]:
        return self._exception_map


def ensure_frkl_exception(exc) -> FrklException:

    if isinstance(exc, FrklException) or issubclass(exc.__class__, FrklException):
        return exc

    return FrklException(msg=exc)
