# -*- coding: utf-8 -*-

from slugify import slugify


def generate_valid_filename(text, sep="_"):

    file_name = slugify(text, separator=sep)

    return file_name
