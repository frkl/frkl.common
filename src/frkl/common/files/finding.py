# -*- coding: utf-8 -*-
import os
import re
from abc import ABCMeta, abstractmethod
from typing import Iterable, Optional

from frkl.common.defaults import DEFAULT_EXCLUDE_DIRS


class FileMatcher(metaclass=ABCMeta):
    @abstractmethod
    def match(self, path: str) -> bool:
        pass


class PathRegexMatcher(FileMatcher):

    _plugin_name = "path_regex"

    def __init__(self, regex: str, match_full_path: bool = False):

        self._regex: str = regex
        self._match_full_path = match_full_path

    def match(self, path: str):

        if self._match_full_path:
            text = path
        else:
            text = os.path.basename(path)
        return re.search(self._regex, text)


def file_matches(
    path: str, matcher_list: Optional[Iterable[FileMatcher]] = None
) -> bool:

    if not matcher_list:
        return True

    for matcher in matcher_list:
        match = matcher.match(path)
        if not match:
            return False

    return True


def find_files(
    base_path: str,
    matchers: Optional[Iterable[FileMatcher]] = None,
    exclude_dirs=DEFAULT_EXCLUDE_DIRS,
) -> Iterable[str]:
    """Find all files in a directory.

    If 'matchers' are provided, only files that match all provided matchers will be returned.
    """

    for root, dirnames, filenames in os.walk(base_path, topdown=True):

        if exclude_dirs:
            dirnames[:] = [d for d in dirnames if d not in exclude_dirs]

        for filename in [
            f
            for f in filenames
            if file_matches(path=os.path.join(root, f), matcher_list=matchers)
        ]:

            path = os.path.join(root, filename)

            yield path
