# -*- coding: utf-8 -*-
import logging
import os
import shutil
from pathspec import PathSpec, patterns
from typing import Any, Iterable, Mapping, Optional, Union

log = logging.getLogger("frkl-common")


def resolve_include_patterns(include_patterns: Optional[Union[str, Iterable[str]]]):

    if not include_patterns:
        _include_patterns: Iterable[str] = ["*", ".*"]
    elif isinstance(include_patterns, str):
        _include_patterns = [include_patterns]
    else:
        _include_patterns = include_patterns

    return _include_patterns


def find_matches(
    path: str,
    include_patterns: Optional[Union[str, Iterable[str]]] = None,
    output_absolute_paths=False,
) -> Iterable:

    _include_patterns = resolve_include_patterns(include_patterns)

    path_spec = PathSpec.from_lines(patterns.GitWildMatchPattern, _include_patterns)

    matches = path_spec.match_tree(path)

    if output_absolute_paths:
        matches = (os.path.join(path, m) for m in matches)

    return matches


def flatten_folder(
    src_path: str,
    target_path: str,
    strategy: Optional[Union[str, Mapping[str, Any]]] = None,
):

    all_files = find_matches(src_path, output_absolute_paths=True)
    for f in all_files:
        target = os.path.join(target_path, os.path.basename(f))
        if os.path.exists(target):
            if strategy == "ignore":
                log.info(f"Duplicate file '{os.path.basename(target)}', ignoring...")
                continue
            else:
                raise NotImplementedError()
        shutil.move(f, target)
