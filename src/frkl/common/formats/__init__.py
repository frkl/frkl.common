# -*- coding: utf-8 -*-
from collections import namedtuple
from enum import Enum

InputFileTypeCharacteristics = namedtuple(
    "InputFileTypeCharacteristics", "local git file folder"
)


class INPUT_FILE_CONTENT_TYPE(Enum):

    local_file = InputFileTypeCharacteristics(
        local=True, git=False, file=True, folder=False
    )
    local_dir = InputFileTypeCharacteristics(
        local=True, git=False, file=False, folder=True
    )
    git_repo = InputFileTypeCharacteristics(
        local=False, git=True, file=False, folder=True
    )
    remote_url = InputFileTypeCharacteristics(
        local=False, git=False, file=True, folder=False
    )


class CONTENT_TYPE(Enum):

    string = 0
    auto = 1
    json = 2
    yaml = 3
    toml = 4


class INPUT_TYPE(Enum):

    unknown = 0
    local_file = 1
    local_dir = 2
    git_repo = 3
    remote_url = 4
    string = 5
    mapping = 6
    iterable = 7


class VALUE_TYPE(Enum):

    unknown = 0
    string = 1
    mapping = 2
    iterable = 3


class OUTPUT_FORMAT(Enum):

    YAML = "yaml"
    RAW = "raw"
    JSON = "json"
    JSON_LINE = "json-line"
    PFORMAT = "pformat"
