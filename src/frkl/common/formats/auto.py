# -*- coding: utf-8 -*-
import collections
import json
import logging
import os
import toml
import typing
from anyio import open_file
from pathlib import Path
from ruamel.yaml import YAML
from typing import Any, Dict, Iterable, Mapping, Optional, Tuple, Type, Union

from frkl.common.downloads import FILE_CONTENT_TYPE, download_file, download_file_async
from frkl.common.exceptions import FrklException, FrklParseException
from frkl.common.formats import (
    CONTENT_TYPE,
    INPUT_FILE_CONTENT_TYPE,
    INPUT_TYPE,
    VALUE_TYPE,
)
from frkl.common.strings import expand_git_url, is_git_repo_url, is_url_or_abbrev
from frkl.common.types import isinstance_or_subclass

log = logging.getLogger("frkl")


def auto_parse_string(
    content: str,
    content_type: Optional[CONTENT_TYPE] = CONTENT_TYPE.auto,
    accepted_result_types: Optional[Iterable[Type]] = None,
) -> Any:
    """Try to automatically parse a string into a Python object.

    If *content_type* is set to 'auto' or None, this will try to parse the provided string as (in this order):

        - "json*
        - *yaml*
        - "toml*

    Optionally, you can provide the *content_origin* argument. This will be used in the execption that is thrown
    should anything go wrong. If the *content_origin* has an attribute 'id', that attribute will be used in
    the exception message instead of the *content_origin* object itself.

    Args:
      content (str): the content to parse
      content_type: the content type, options: 'auto', 'yaml', 'json', 'toml'
      *accepted_result_types (Type): if specified, the parsed result object needs to be of of one of the supplied types, otherwise any type is fine

    Returns:
        Union[List, Dict]: either a list or dict, represented by the input string. If the input string is empty, None is returned.
    """

    loaded = None

    if content_type is None:
        content_type = CONTENT_TYPE.auto

    if not isinstance(content_type, CONTENT_TYPE):
        raise Exception("Input is not a string, but '{}'".format(type(content)))

    if not content:
        return None

    if content_type == CONTENT_TYPE.string:
        return content

    possible_exceptions: Dict[CONTENT_TYPE, Exception] = {}

    parsed_content_type: Optional[CONTENT_TYPE] = None
    if content_type == CONTENT_TYPE.auto:

        parsed_content_type = CONTENT_TYPE.json
        try:
            loaded = json.loads(content)
        except (Exception) as e:
            content_type = None
            possible_exceptions[CONTENT_TYPE.json] = e

        if loaded is None:
            parsed_content_type = CONTENT_TYPE.yaml
            try:
                yaml = YAML()
                loaded = yaml.load(content)
            except (Exception) as e:
                content_type = None
                possible_exceptions[CONTENT_TYPE.yaml] = e

        if loaded is None:
            parsed_content_type = CONTENT_TYPE.toml
            try:
                loaded = toml.loads(content)
            except (Exception) as e:
                content_type = None
                possible_exceptions[CONTENT_TYPE.toml] = e

        if loaded is None:

            raise FrklParseException(
                content=content,
                msg="Could not parse string.",
                reason="Unknown format, tried 'yaml', 'json', and 'toml'.",
                exception_map=possible_exceptions,
            )

        if len(content) > 31:
            snippet = (content[0:30] + "...").replace("\n", " ")
        else:
            snippet = content
        log.debug(
            "Detected content type for '{}': {}".format(snippet, parsed_content_type)
        )

    else:
        parsed_content_type = content_type
        if content_type == CONTENT_TYPE.yaml:
            try:
                yaml = YAML()
                loaded = yaml.load(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    msg="Could not parse string as YAML.",
                    reason="String is not valid YAML.",
                    exception_map={CONTENT_TYPE.yaml: e},
                )
        elif content_type == CONTENT_TYPE.json:

            try:
                loaded = json.loads(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    msg="Could not parse string as JSON.",
                    reason="String is not valid JSON.",
                    exception_map={CONTENT_TYPE.json: e},
                )
        elif content_type == CONTENT_TYPE.toml:
            try:
                loaded = toml.loads(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    msg="Could not parse string as TOML.",
                    reason="String is not valid TOML.",
                    exception_map={CONTENT_TYPE.toml: e},
                )
        else:
            raise FrklParseException(
                content=content,
                msg="Could not parse content.",
                reason=f"Unsupported requested content type for string: {content_type}",
                solution="Check format of content and specify correct content_type.",
            )

    if accepted_result_types:
        accepted = False
        for art in accepted_result_types:
            print(art)
            if isinstance_or_subclass(loaded, art):
                accepted = True
                break

        if not accepted:
            raise FrklParseException(
                content=content,
                msg="Coult not parse content.",
                reason=f"Resulting type of parsed content ({type(loaded)}) not among the accepted result types: {accepted_result_types}.",
            )

    log.debug(
        f"successfully parsed string -> parse_type={parsed_content_type}, result_type={type(loaded)}"
    )

    return loaded


def determine_input_file_type(
    input_obj: Union[str, Path]
) -> Optional[INPUT_FILE_CONTENT_TYPE]:
    """Determine the type of the provided string or path."""

    if isinstance(input_obj, (str, Path)):

        if isinstance(input_obj, str):
            path = Path(input_obj)
        else:
            path = input_obj

        if path.exists():
            if path.is_file() or path.is_symlink() or path.is_fifo():
                return INPUT_FILE_CONTENT_TYPE.local_file
            elif path.is_dir():
                return INPUT_FILE_CONTENT_TYPE.local_dir

        if isinstance(input_obj, str):

            url = expand_git_url(input_obj)
            if is_git_repo_url(url):
                return INPUT_FILE_CONTENT_TYPE.git_repo
            elif is_url_or_abbrev(url):
                return INPUT_FILE_CONTENT_TYPE.remote_url

    return None


def determine_input_type(input_obj: Any) -> INPUT_TYPE:
    """Determine the type of an input object.

    Possible result values (tested in that order):
     - local_file: if a local (existing) file
     - local_dir: if a local (existing) dir
     - git_repo: if a valid git url
     - remote_url: if a valid url
     - string: if input is of type string
     - dict: if input is of type Mapping
     - list: if input is of type Iterable
     - None: if none of the above types match
    """

    ft = determine_input_file_type(input_obj)

    if ft is not None:
        return INPUT_TYPE[ft.name]

    if isinstance(input_obj, str):
        return INPUT_TYPE.string
    elif isinstance(input_obj, collections.abc.Mapping):
        return INPUT_TYPE.mapping
    elif isinstance(input_obj, collections.abc.Iterable):
        return INPUT_TYPE.iterable

    return INPUT_TYPE.unknown


def get_content(
    input_obj: Any,
    input_type: INPUT_TYPE = None,
    content_type: Optional[CONTENT_TYPE] = CONTENT_TYPE.auto,
    value_key: Optional[str] = None,
    type_key: Optional[str] = None,
    metadata_key: Optional[str] = None,
    cache_base: Optional[Union[str, Path]] = None,
) -> Any:
    """Auto determine content of the given input object.

    Automatically reads/downloads content in case the input object is a path or url string. If 'value_key' is not provided,
    the return value is the parsed content of the input object (converted to a dict or list if possible). If provided,
    the result is a dict that includes metadata about the input object and parsing process.

    Args:
      input_obj: the input object
      input_type: the input type (will be auto-detected if not provided)
      content_type: the type of the content (e.g. 'yaml', will be auto determined if not provided) - only applies to input types that have content, like a file
      value_key (Optional[str]): if set, return a dict using this key for the content value
      type_key (Optional[str]): if set, and 'value_key' is set, use this key to set the value type of the result value in the returning dict
      metadata_key (Optional[str]): if set, and 'value_key' is set, use this key to set the metadata of the result value in the returning dict
      cache_base (Optional[str, Path]): if set, and input is of type 'remote_url', use a cached download
    Returns:
        the content as a dict (if possible), or string, optionally wrapped in a dict containing metadata about the content and parse process
    """

    if input_type is None:
        input_type = determine_input_type(input_obj)

    if content_type is None:
        content_type = CONTENT_TYPE.auto

    value: Any = None
    metadata: Dict[str, Any] = {}
    if input_type is INPUT_TYPE.unknown:
        value = input_obj
        value_type: VALUE_TYPE = VALUE_TYPE.unknown
    elif input_type == INPUT_TYPE.string:
        content = input_obj
        try:
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except FrklParseException as fpe:
            raise fpe
        except Exception:
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False
    elif input_type == INPUT_TYPE.mapping:
        value = input_obj
        value_type = VALUE_TYPE.mapping
    elif input_type == INPUT_TYPE.iterable:
        value = input_obj
        value_type = VALUE_TYPE.iterable
    elif input_type == INPUT_TYPE.local_file:
        content = Path(input_obj).read_text()
        try:
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False
    elif input_type == INPUT_TYPE.local_dir:
        raise Exception("Can't get content from local directory.")
    elif input_type == INPUT_TYPE.git_repo:
        raise Exception("Can't get content from git repository.")
    elif input_type == INPUT_TYPE.remote_url:
        r_url: str = expand_git_url(
            input_obj
        )  # doesn't need to be git url, but just in case...
        metadata["full_url"] = r_url
        if not cache_base:
            content = download_file(
                r_url, return_content=True, content_type=FILE_CONTENT_TYPE.text
            )  # type: ignore
        else:
            raise NotImplementedError()
        try:
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False

    if not value_key:
        return value

    result = {}
    result[value_key] = value

    if type_key is not None:
        if type_key == value_key:
            raise Exception(f"Type and value keys are the same: {value_key}")
        result[type_key] = value_type

    if metadata_key is not None:
        result[metadata_key] = metadata

    return result


async def get_content_async(
    input_obj: Any,
    input_type: INPUT_TYPE = None,
    content_type: Optional[CONTENT_TYPE] = CONTENT_TYPE.auto,
    value_key: Optional[str] = None,
    type_key: Optional[str] = None,
    metadata_key: Optional[str] = None,
    cache_base: Optional[Union[str, Path]] = None,
) -> Any:
    """Auto determine content of the given input object (async version).

    Automatically reads/downloads content in case the input object is a path or url string. If 'value_key' is not provided,
    the return value is the parsed content of the input object (converted to a dict or list if possible). If provided,
    the result is a dict that includes metadata about the input object and parsing process.

    Args:
      input_obj: the input object
      input_type: the input type (will be auto-detected if not provided)
      content_type: the type of the content (e.g. 'yaml', will be auto determined if not provided) - only applies to input types that have content, like a file
      value_key (Optional[str]): if set, return a dict using this key for the content value
      type_key (Optional[str]): if set, and 'value_key' is set, use this key to set the value type of the result value in the returning dict
      metadata_key (Optional[str]): if set, and 'value_key' is set, use this key to set the metadata of the result value in the returning dict
      cache_base (Optional[str, Path]): if set, and input is of type 'remote_url', use a cached download
    Returns:
        the content as a dict (if possible), or string, optionally wrapped in a dict containing metadata about the content and parse process
    """

    if input_type is None:
        input_type = determine_input_type(input_obj)

    if content_type is None:
        content_type = CONTENT_TYPE.auto

    value: Any = None
    metadata: Dict[str, Any] = {}
    if input_type is INPUT_TYPE.unknown:
        value = input_obj
        value_type: VALUE_TYPE = VALUE_TYPE.unknown
    elif input_type == INPUT_TYPE.string:
        content = input_obj
        try:
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except FrklParseException as fpe:
            raise fpe
        except Exception:
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False
    elif input_type == INPUT_TYPE.mapping:
        value = input_obj
        value_type = VALUE_TYPE.mapping
    elif input_type == INPUT_TYPE.iterable:
        value = input_obj
        value_type = VALUE_TYPE.iterable
    elif input_type == INPUT_TYPE.local_file:
        try:
            async with await open_file(input_obj) as f:
                content = await f.read()
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False
    elif input_type == INPUT_TYPE.local_dir:
        raise Exception("Can't get content from local directory.")
    elif input_type == INPUT_TYPE.git_repo:
        raise Exception("Can't get content from git repository.")
    elif input_type == INPUT_TYPE.remote_url:
        r_url: str = expand_git_url(
            input_obj
        )  # doesn't need to be git url, but just in case...
        metadata["full_url"] = r_url
        if not cache_base:
            content = await download_file_async(
                r_url, return_content=True, content_type=FILE_CONTENT_TYPE.text
            )  # type: ignore
        else:
            raise NotImplementedError()
        try:
            value, value_type = parse_content_string(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = VALUE_TYPE.string
            metadata["parse_successful"] = False

    if not value_key:
        return value

    result = {}
    result[value_key] = value

    if type_key is not None:
        if type_key == value_key:
            raise Exception(f"Type and value keys are the same: {value_key}")
        result[type_key] = value_type

    if metadata_key is not None:
        result[metadata_key] = metadata

    return result


def parse_content_string(
    content: str, content_type: CONTENT_TYPE = CONTENT_TYPE.auto
) -> Tuple[Any, VALUE_TYPE]:
    """Parse the provided string.

    Args:
        content (str): a string
        content_type (CONTENT_TYPE): the type of interpreted value of the string

    Returns:
        Tuple[Any, INPUT_TYPE]: the interpreted value, and the type of the interpreted value
    """

    if content_type is None:
        content_type = CONTENT_TYPE.auto

    if content_type == CONTENT_TYPE.string:
        return content, VALUE_TYPE.string

    parsed: Any = auto_parse_string(content, content_type=content_type)

    if isinstance_or_subclass(parsed, str):
        value_type = VALUE_TYPE.string
    elif isinstance(parsed, collections.abc.Mapping):
        value_type = VALUE_TYPE.mapping
    elif isinstance(parsed, collections.abc.Iterable):
        value_type = VALUE_TYPE.iterable
    else:
        value_type = VALUE_TYPE.unknown

    return parsed, value_type


class AutoInput(object):
    """Class to automatically determine and read (as much as possible) input from strings, paths, urls, etc...

    TODO: more documentation
    """

    def __init__(
        self,
        input_value: Any,
        input_type: Optional[INPUT_TYPE] = None,
        content_type: Optional[CONTENT_TYPE] = None,
    ) -> None:

        self._input_value: Any = input_value
        if input_type is None:
            input_type = determine_input_type(input_value)
        self._input_type: INPUT_TYPE = input_type
        if content_type is None:
            content_type = CONTENT_TYPE.auto
        self._content_type: CONTENT_TYPE = content_type

        self._content: Optional[Mapping[str, Any]] = None

    @property
    def input_value(self):

        return self._input_value

    # @input_value.setter
    # def input_value(self, input_value):
    #
    #     self._input_type = None
    #     self._content = None
    #     self._intput_value = input_value

    @property
    def input_type(self):

        return self._input_type

    def get_value_type(self) -> VALUE_TYPE:

        if self._content is None:
            self.get_content()
        value_type = self._content["value_type"]  # type: ignore

        return value_type

    async def get_value_type_async(self) -> VALUE_TYPE:

        if self._content is None:
            await self.get_content_async()

        value_type = self._content["value_type"]  # type: ignore
        return value_type

    async def get_content_async(self) -> Mapping[str, Any]:

        if self._content is not None:
            return self._content["value"]

        self._content = await get_content_async(
            input_obj=self.input_value,
            input_type=self.input_type,
            content_type=self._content_type,
            value_key="value",
            type_key="value_type",
            metadata_key="metadata",
        )

        # if self._content_type != CONTENT_TYPE.auto:
        #     content_type = self._content["value_type"]
        #     if content_type != self._content_type:
        #         raise FrklException(
        #             reason=f"Invalid content type: {content_type.name} != {self._content_type.name}"
        #         )

        return self._content["value"]

    def get_content(self) -> Mapping[str, Any]:

        if self._content is not None:
            return self._content

        self._content = get_content(
            input_obj=self.input_value,
            input_type=self.input_type,
            content_type=self._content_type,
            value_key="value",
            type_key="content_type",
            metadata_key="metadata",
        )
        if self._content_type != CONTENT_TYPE.auto:
            content_type = self._content["content_type"]
            if content_type != self._content_type:
                raise FrklException(
                    reason=f"Invalid content type: {content_type.name} != {self._content_type.name}"
                )

        return self._content["value"]

    def __repr__(self):

        resolved = False
        msg = ""
        if self._content is not None:
            resolved = True
            msg = f" input_type={self.input_type}, content_type={self._content['content_type'].name}"

        return f"(SmartInput: input={self._input_value}, parsed={resolved}{msg})"


async def create_object_from_list_of_strings(
    *tokens,
    allow_remote: bool = False,
    non_dict_ok: bool = True,
    iterator_value_keys: typing.Optional[typing.Iterator[str]] = None,
    duplicate_key_strategy: typing.Literal[
        "overwrite", "keep_original", "forbid"
    ] = "overwrite",
) -> Dict[str, Any]:
    """Create a dictionary (or other types) from a list of strings.

    The strings can be:
    - a key/value pair, seperated by '=' (the value again can be all of the allowed formats listed here)
    - valid json
    - a path to a file containing json or yaml content
    - a url to a file (if 'allow_remote' is set to True)

    Arguments:
        *tokens: a list of strings
        allow_remote: whether to allow remote content
        scalar_ok: whether the result can also be a scalar
        non_dict_ok: whether it's ok for the result to be a type other than a dict, this is mostly for the internal recursion
        iterator_value_keys: a list of keys to indicate that the values in them should be lists, not single items
        duplicate_key_strategy: what to do when encountering a key for a dictionary result (does not apply if the key is in 'iterator_value_keys' or when both values are dicts)
    """

    if not tokens:
        return {}

    if allow_remote:
        raise NotImplementedError()

    result: Dict[str, Any] = {}
    for token in tokens:
        if "=" in token:
            key, value = token.split("=", maxsplit=1)
            if "." in key:
                raise NotImplementedError()
            _v = create_object_from_list_of_strings(
                value, allow_remote=allow_remote, non_dict_ok=True
            )
            part_config = {key: _v}
        elif os.path.isfile(os.path.expanduser(token)):
            path = os.path.realpath(os.path.expanduser(token))
            part_config = get_content_async(path)
            if not non_dict_ok:
                if not isinstance(part_config, Mapping):
                    raise FrklException(
                        f"Can't convert file '{token}]' to dictionary.",
                        reason="File content not a 'mapping' type.",
                    )

        else:
            try:
                part_config = json.loads(token)
                if not non_dict_ok:
                    if not isinstance(part_config, Mapping):
                        raise FrklException(
                            f"Can't convert json string '{token}]' to dictionary.",
                            reason="Not a 'mapping' type.",
                        )

            except Exception:
                raise Exception(f"Could not parse argument into data: {token}")

        if iterator_value_keys is None:
            iterator_value_keys = []

        for k, v in part_config.items():

            if k in iterator_value_keys:
                result.setdefault(k, []).append(v)
            else:
                if k in result.keys():
                    if isinstance(result[k], typing.Mapping):
                        raise NotImplementedError()
                    if duplicate_key_strategy == "overwrite":
                        result[k] = v
                    elif duplicate_key_strategy == "keep_original":
                        pass
                    elif duplicate_key_strategy == "forbid":
                        raise FrklException(
                            msg=f"Can't parse tokens into dict.",
                            reason=f"Duplicate key '{k}', and duplicate key strategy set to 'forbid'.",
                        )
                else:
                    result[k] = v

        return result
