# -*- coding: utf-8 -*-
import collections
import json
import pprint
from collections import OrderedDict
from pathlib import Path
from ruamel.yaml import RoundTripRepresenter
from ruamel.yaml.comments import CommentedMap
from typing import Any, Mapping, Optional, Union

from frkl.common.exceptions import FrklException
from frkl.common.formats import OUTPUT_FORMAT
from frkl.common.formats.yaml import StringYAML
from frkl.common.strings import reindent, reindent as rind


def is_explanation(obj: Any) -> bool:

    if hasattr(obj, "explanation_data"):
        return True
    else:
        return False


class IgnoreAliasRepresenter(RoundTripRepresenter):
    def __init__(self, default_style=None, default_flow_style=None, dumper=None):

        super(IgnoreAliasRepresenter, self).__init__(
            default_style=default_style,
            default_flow_style=default_flow_style,
            dumper=dumper,
        )

    def ignore_aliases(self, data):
        return True


def serialize(
    python_object: Any,
    format: Union[OUTPUT_FORMAT, str] = "raw",
    target: Optional[Union[str, Mapping]] = None,
    safe=False,
    indent=0,
    sort_keys=False,
    ignore_aliases=False,
    yaml_representers=None,
    strip: bool = False,
):
    """Utility method to print out readable strings from python objects (mostly dicts).

    Args:
        python_object (obj): the object to print
        format (OUTPUT_FORMAT, str): the format of the output (available: 'yaml', 'json', 'raw', and 'pformat')
        target:
        safe (bool): whether to use a 'safe' way of converting to string (if available in the output format type)
        indent (int): the indentation (optional)
        sort_keys (boolean): whether to sort a (root-level) dictionary (only works for YAML so far)
        strip (bool): whether to strip the result before indenting
    """

    if format is None:
        format = OUTPUT_FORMAT.RAW

    if isinstance(format, str):
        try:
            format = OUTPUT_FORMAT(format)
        except (Exception):

            raise Exception(
                f"No valid output format provided. Supported: {', '.join([e.value for e in OUTPUT_FORMAT])}"
            )

    if format == OUTPUT_FORMAT.YAML:

        if isinstance(python_object, OrderedDict) or (
            sort_keys and isinstance(python_object, Mapping)
        ):
            temp = CommentedMap()
            if sort_keys:
                for k in sorted(python_object):
                    temp[k] = python_object[k]
            else:
                for k in python_object:
                    temp[k] = python_object[k]
        else:
            temp = python_object

        if safe:
            ryaml = StringYAML(typ="safe")
            if yaml_representers:
                for k, v in yaml_representers.items():
                    ryaml.representer.add_representer(k, v)
            ryaml.default_flow_style = False
            output_string = ryaml.dump(temp)
            # output_string = yaml.safe_dump(
            # python_object,
            # default_flow_style=False,
            # encoding='utf-8',
            # allow_unicode=True)
        else:
            ryaml = StringYAML()
            if yaml_representers:
                for k, v in yaml_representers.items():
                    ryaml.representer.add_representer(k, v)
            if ignore_aliases:
                ryaml.Representer = IgnoreAliasRepresenter
            ryaml.default_flow_style = False
            output_string = ryaml.dump(temp)

            # output_string = yaml.dump(
            # python_object,
            # default_flow_style=False,
            # encoding='utf-8',
            # allow_unicode=True)
    elif format == OUTPUT_FORMAT.JSON:
        output_string = json.dumps(python_object, sort_keys=sort_keys, indent=2)
    elif format == OUTPUT_FORMAT.JSON_LINE:
        output_string = json.dumps(python_object, sort_keys=sort_keys, indent=None)
    elif format == OUTPUT_FORMAT.RAW:
        output_string = str(python_object)
    elif format == OUTPUT_FORMAT.PFORMAT:
        output_string = pprint.pformat(python_object)

    if strip:
        output_string = output_string.strip()

    if indent != 0:
        output_string = reindent(output_string, indent)

    if target:
        if isinstance(target, str):
            write_string_to(output_string, target=target)
        elif isinstance(target, Mapping):
            target_val = target.get("target", None)
            target_type = target.get("target_type", None)
            target_opts = target.get("target_opts", {})
            write_string_to(
                output_string, target=target_val, target_type=target_type, **target_opts
            )
        else:
            raise TypeError(
                f"'target' value, needs to be either str or Mapping, not: {type(target)}"
            )

    return output_string


def write_string_to(
    content: str, target: Any = None, target_type: str = "auto", **target_opts: Any
):

    if (target_type is None or target_type == "auto") and target is None:
        target_type = "return-value"
    elif target_type is None or target_type == "auto":
        target_type = "file"

    if target_type == "return-value":
        return content

    if target_type != "file":
        raise NotImplementedError(
            "Only 'file' and 'return-value' write target types supported at this stage."
        )

    target_config = {"force": False}
    target_config.update(target_opts)

    path = Path(target).resolve()  # type: ignore

    if path.exists():
        if path.is_dir():
            raise FrklException(
                msg=f"Can't write content to path '{path.as_posix()}'",
                reason="Target is a directory.",
            )
        if not target_config["force"]:
            raise FrklException(
                msg=f"Can't write content to path '{path.as_posix()}'",
                reason="Target exists and 'force' set to 'False'.",
            )

    path.write_text(content)

    return {"target": path}


def make_serializable(value: Any) -> Any:

    result: Any = None
    if value is None:
        result = "-- no value --"
    elif isinstance(value, str):
        result = value
    elif isinstance(value, bool):
        result = "true" if value else "false"
    elif isinstance(value, (int, float)):
        result = str(value)
    elif is_explanation(value):
        result = value.serializable_data
    elif hasattr(value, "explain"):
        try:
            temp = value.explain()
            if is_explanation(temp):
                result = temp.serializable_data
        except Exception:
            pass
    elif hasattr(value, "to_dict"):
        try:
            temp = value.to_dict()
            result = make_serializable(temp)
        except Exception:
            pass

    if result is None:
        if isinstance(value, collections.abc.Mapping):
            result = {}
            for k, v in value.items():
                r_k = make_serializable(k)
                r_v = make_serializable(v)
                result[r_k] = r_v
        elif isinstance(value, collections.abc.Iterable):
            result = []
            for r in value:
                result.append(make_serializable(r))
        elif hasattr(value, "to_dict"):
            result = value.to_dict()  # type: ignore
        else:
            result = str(value)

    return result


def to_value_string(value: Any, reindent: int = 0) -> str:

    r = make_serializable(value)

    if isinstance(r, str):
        r_str = r
    elif isinstance(r, (collections.abc.Sequence, collections.abc.Mapping)):
        r_str = serialize(r, format="yaml")
    else:
        r_str = str(r)

    if reindent:
        r_str = rind(r_str, reindent)

    return r_str
