# -*- coding: utf-8 -*-

# flake8: noqa

import logging
import os
from shutil import which

log = logging.getLogger("frkl-common.git")

git_override = os.environ.get("USE_INTERNAL_GIT", None)
if not git_override:
    git_path = which("git")
    if git_path:
        use_internal_git = False
    else:
        use_internal_git = True
else:
    if git_override.lower() == "true":
        use_internal_git = True
    else:
        use_internal_git = False

if not use_internal_git:
    log.debug("using external git command...")
else:
    log.debug("using internal git command...")
