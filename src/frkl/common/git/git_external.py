# -*- coding: utf-8 -*-
import logging
import os
import shutil
from git.objects.util import from_timestamp
from pydriller import GitRepository
from typing import Any, Dict, Mapping, MutableMapping, Optional

from frkl.common.async_utils import wrap_async_task
from frkl.common.defaults import FRKL_GIT_CHECKOUT_CACHE
from frkl.common.downloads.cache import calculate_cache_path
from frkl.common.exceptions import FrklException
from frkl.common.filesystem import ensure_folder
from frkl.common.strings import generate_valid_identifier
from frkl.common.subprocesses import GitProcess

log = logging.getLogger("bring")


async def ensure_repo_cloned(url, update=False) -> str:

    path = calculate_cache_path(base_path=FRKL_GIT_CHECKOUT_CACHE, url=url)
    parent_folder = os.path.dirname(path)

    exists = False
    if os.path.exists(path):
        exists = True

    if exists and not update:
        return path

    ensure_folder(parent_folder)

    if not exists:
        # clone to a temp location first, in case another process tries to do the same
        temp_name = generate_valid_identifier()
        temp_path = os.path.join(parent_folder, temp_name)
        git = GitProcess(
            "clone", url, temp_path, working_dir=parent_folder, GIT_TERMINAL_PROMPT="0"
        )

        await git.run(wait=True)

        if os.path.exists(path):
            shutil.rmtree(temp_path, ignore_errors=True)
        else:
            shutil.move(temp_path, path)

    else:
        # TODO: some sort of lock?
        git = GitProcess("fetch", working_dir=path)

        await git.run(wait=True)

    return path


async def clone_local_repo_async(
    source_repo: str, target_path: str, version: Optional[str] = None
) -> None:

    if os.path.exists(target_path):
        raise FrklException(
            "Can't clone local git repo.",
            reason=f"Target path already exists: {target_path}",
        )

    try:
        parent_folder = os.path.dirname(target_path)
        ensure_folder(parent_folder)

        temp_name = generate_valid_identifier()
        temp_path = os.path.join(parent_folder, temp_name)

        shutil.copytree(source_repo, temp_path)

        if version:
            git = GitProcess(
                "checkout", version, working_dir=temp_path, GIT_TERMINAL_PROMPT="0"
            )

            await git.run(wait=True)

    except Exception as e:
        log.debug(
            f"Can't clone local git repo {source_repo} -> {target_path}", exc_info=True
        )
        raise e

    if os.path.exists(target_path):
        shutil.rmtree(temp_path, ignore_errors=True)
        raise FrklException(
            "Can't clone local git repo.",
            reason=f"Target repository create during cloning process: {target_path}",
        )
    else:
        shutil.move(temp_path, target_path)


def clone_local_repo(
    source_repo: str, target_path: str, version: Optional[str] = None
) -> None:

    if os.path.exists(target_path):
        raise FrklException(
            "Can't clone local git repo.",
            reason=f"Target path already exists: {target_path}",
        )

    wrap_async_task(clone_local_repo_async, source_repo, target_path, version)


def get_repo_info(local_path: str) -> Mapping[str, Any]:

    gr = GitRepository(local_path)

    commits = get_commits_info(gr)
    tags = get_tags_from_repo(gr)
    branches = get_branches_from_repo(gr)

    result: Dict[str, Any] = {"branches": branches, "tags": tags, "commits": commits}

    return result


def get_commits_info(gr: GitRepository) -> Mapping[str, Mapping[str, Any]]:

    commits: MutableMapping[str, Mapping[str, Any]] = {}
    for c in gr.get_list_commits():

        auth_date = from_timestamp(
            c._c_object.authored_date, c._c_object.author_tz_offset
        )

        # auth_date_string = auth_date.format("ddd MMM DD YYYY HH:mm:ss")

        commits[c.hash] = {"release_date": str(auth_date)}
    return commits


def get_branches_from_repo(gr: GitRepository) -> Mapping[str, str]:
    """Retrieve all branches from provided git repo.

    Returns:
        Mapping: a dict with the branch name as key, and the commit hash of its head as value
    """

    branches: Dict[str, str] = {}
    for b in gr.repo.branches:
        branches[b.name] = b.commit.hexsha

    return branches


def get_tags_from_repo(gr: GitRepository) -> Mapping[str, str]:
    """Retrieve all tags from provided git repo.

    Returns:
        Mapping: a dict with the tag name as key, and the commit hash it refers to as value
    """

    tags: Dict[str, str] = {}
    for t in reversed(sorted(gr.repo.tags, key=lambda t: t.commit.committed_datetime)):
        tags[t.name] = t.commit.hexsha

    return tags
