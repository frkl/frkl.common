# -*- coding: utf-8 -*-

# from jinja2schema import Config, infer_from_ast, parse
# from jinja2schema.model import (
#     Boolean,
#     Dictionary,
#     Number,
#     Scalar,
#     String,
#     Tuple,
#     Unknown,
#     Variable,
# )


import jinja2
import json
import logging
import typing
from jinja2 import Environment, TemplateSyntaxError, meta
from jinja2.nativetypes import NativeEnvironment
from markupsafe import Markup
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Sequence,
    Set,
    Type,
    Union,
)

log = logging.getLogger("frkl")
JINJA_DELIMITER_PROFILES: Dict[str, Dict[str, str]] = {
    "default": {
        "block_start_string": "{%",
        "block_end_string": "%}",
        "variable_start_string": "{{",
        "variable_end_string": "}}",
    },
    "frkl": {
        "block_start_string": "{%::",
        "block_end_string": "::%}",
        "variable_start_string": "{{::",
        "variable_end_string": "::}}",
    },
    "shell": {
        "block_start_string": "#%::",
        "block_end_string": "#::%",
        "variable_start_string": "#{{::",
        "variable_end_string": "::}}#",
    },
}
VALID_ENV_TYPES = ["default", "native"]
DEFAULT_ENVS: Dict[str, Environment] = {}


def get_global_jinja_env(
    delimiter_profile: str = "default", env_type: str = "default"
) -> Environment:
    """Manages default jinja environments.

    In a lot of cases it's not worth creating new ones nor threading them through function invocations.
    """

    key = f"{delimiter_profile}_{env_type}"

    if key in DEFAULT_ENVS.keys():
        return DEFAULT_ENVS[key]

    env = create_jinja_environment(delimiter_profile, env_type)
    DEFAULT_ENVS[key] = env
    return DEFAULT_ENVS[key]


def create_jinja_environment(
    delimiter_profile: str = "default", env_type: str = "default", **filters
) -> Environment:
    """Utility method to create jinja environments."""

    if delimiter_profile not in JINJA_DELIMITER_PROFILES.keys():
        raise Exception(f"Invalid delimiter profile: {delimiter_profile}")

    if env_type not in VALID_ENV_TYPES:
        raise Exception(
            f"Invalid jinja environment type '{env_type}, valid: {VALID_ENV_TYPES}"
        )

    if env_type == "default":
        env = Environment(**JINJA_DELIMITER_PROFILES[delimiter_profile])  # type: ignore
    else:
        env = NativeEnvironment(
            **JINJA_DELIMITER_PROFILES[delimiter_profile]
        )  # type: ignore

    return env


def jinja_env_or_default(jinja_env: Optional[Environment] = None) -> Environment:
    """Utility method to return the default jinja env if the provided one is `None`."""

    if jinja_env is None:
        return get_global_jinja_env()
    else:
        return jinja_env


def get_template_keys(
    data,
    jinja_env: Environment = None,
    ignore_unknown_types: bool = True,
    init_keys: Set = None,
) -> Set[str]:
    """Gets a list of used template keys in all keys/values of a python object.

    Args:
        - *data*: the input
        - *jinja_env*: the delimiter profile
        - *ignore_unknown_types*: whether to ignore unknown types (types that are not str, Mapping, Sequence)
        - *init_keys*: initial set of keys, used for recursion

    Returns:
        - a set of template keys
    """

    if init_keys is None:
        init_keys = set()

    if not data:
        return init_keys
    if isinstance(data, str):
        keys = get_template_keys_from_string(data, jinja_env=jinja_env)
        init_keys.update(keys)

        return init_keys
    elif isinstance(data, Sequence):
        for d in data:
            get_template_keys(
                d,
                jinja_env=jinja_env,
                ignore_unknown_types=ignore_unknown_types,
                init_keys=init_keys,
            )
        return init_keys
    elif isinstance(data, Mapping):
        for k, v in data.items():
            get_template_keys(
                k,
                jinja_env=jinja_env,
                ignore_unknown_types=ignore_unknown_types,
                init_keys=init_keys,
            )
            get_template_keys(
                v,
                jinja_env=jinja_env,
                ignore_unknown_types=ignore_unknown_types,
                init_keys=init_keys,
            )
        return init_keys
    else:
        if ignore_unknown_types:
            return init_keys
        else:
            raise Exception("Can't get template keys from type '{}'".format(type(data)))


def string_is_templated(text: str, jinja_env: Environment = None) -> bool:
    """Utility method to determine whether a string has template markers in it.

    This is pretty simplistic, it only checks whether one of the template marker strings
    (e.g. '}}', or '{%') are contained in the text. It doesn't check for matching opening/
    closed brackets etc.

    Args:
      - *text*: the text in question
      - *jinja_env*: the jinja environment

    Returns:
      - whether the text is templated or not
    """

    if not isinstance(text, str):
        return False

    jinja_env = jinja_env_or_default(jinja_env)

    block_start_string = jinja_env.block_start_string
    if block_start_string in text:
        return True
    variable_start_string = jinja_env.variable_start_string
    if variable_start_string in text:
        return True

    return False


def get_template_keys_from_string(
    text: str, jinja_env: Environment = None
) -> Iterable[str]:
    """Retrieves all template keys that are contained in a string.

    Args:
      - *text*: the text in question
      - *jinja_delimiter_profile*: the delimiter profile

    Returns:
      - a list of strings
    """

    if not isinstance(text, str) or not string_is_templated(text, jinja_env=jinja_env):
        return []

    jinja_env = jinja_env_or_default(jinja_env)

    try:
        ast = jinja_env.parse(text)
        result = meta.find_undeclared_variables(ast)

        return result
    except (TemplateSyntaxError) as e:
        log.debug(str(e.__dict__))
        raise e


# def get_template_schema(template: Any, jinja_env=None) -> Dict[str, Variable]:
#     """Return a schema for the variables inside a provided template.
#
#     If the template is empty, an empty dict will be returned. This really only is tested with str, Mapping and Sequence
#     objects as templates.
#
#     This uses the `jinja2schema` library under the hood.
#
#     Args:
#         - *template*: the jinja template
#         - *jinja_env*: a custom jinja env (optional)
#     """
#
#     if not template:
#         return {}
#
#     schema = infer_from_ast(
#         parse(template, jinja2_env=jinja_env_or_default(jinja_env)),
#         ignore_constants=True,
#         prototing_config=Config(IGNORE_UNKNOWN_FILTER=True),
#     )
#
#     return schema
#
#
# VARIABLE_TYPE_MAP = {
#     Boolean: "boolean",
#     Dictionary: "dict",
#     String: "string",
#     Number: "float",
#     Unknown: "any",
#     Scalar: "any",
#     Tuple: "list",
#     List: "list",
# }
#
#
# def template_schema_to_args(
#     template_schema: typing.Mapping[str, Variable]
# ) -> typing.Mapping[str, typing.Mapping[str, Any]]:
#
#     result = {}
#     for k, v in template_schema.items():
#         if v.constant:
#             continue
#         temp = {}
#         temp["required"] = v.required
#         if v.value is not None:
#             temp["default"] = v.value
#
#         temp["type"] = VARIABLE_TYPE_MAP[v.__class__]
#
#         result[k] = temp
#
#     return result
#
#
# def find_args_in_obj(
#     template_obj: Any, jinja_env: Optional[Environment] = None
# ) -> typing.Mapping[str, Any]:
#
#     schema = get_template_schema(template=template_obj, jinja_env=jinja_env)
#     args = template_schema_to_args(schema)
#     return args


def process_string_template(
    template_string: str,
    replacement_dict: Optional[typing.Mapping[str, Any]] = None,
    jinja_env: Optional[Environment] = None,
):
    """Replace template markers with values from a replacement dictionary within a string.

    Args:
      - *template_string*: the template string
      - *replacement_dict*: the dictionary with the replacement strings
      - *jinja_env*: an existing jinja env to use, if specified, the following paramters will be ignored
    """

    if replacement_dict is None:
        sub_dict = {}
    else:
        sub_dict = dict(replacement_dict)

    if jinja_env is not None:
        env = jinja_env
    else:
        env = get_global_jinja_env()

    if not string_is_templated(template_string, env):
        return template_string

    for k, v in sub_dict.items():
        match = "{} {} {}".format(env.variable_start_string, k, env.variable_end_string)
        if template_string == match:
            return v

    if isinstance(template_string, jinja2.runtime.Undefined):
        return ""

    # add some keywords, to make sure we don't get any weird internal result objects
    sub_dict.setdefault("namespace", None)

    result = env.from_string(template_string).render(sub_dict)

    # some manual sanity checks for edge cases
    if isinstance(result, Markup):
        result = str(result)

    if isinstance(result, jinja2.runtime.Undefined):
        result = ""

    if "tojson" in template_string and isinstance(result, Mapping):
        log.debug(
            "Converting result to json string manually, this is a workaround because Jinja returns the wrong type in this case."
        )
        result = json.dumps(result)
        return result

    return result


def replace_strings_in_obj(
    source_obj: Any, replacement_dict: Dict = None, jinja_env: Environment = None
) -> Union[MutableMapping, str, Sequence]:
    """Replace keys/values with template values within an object.

    strings, dicts and lists are supported (for now), everything else is ignored.

    Args:
      source_obj (object): the source object containing the strings to replace
      replacement_dict (dict): the dictionary with the replacement strings
      jinja_env: an existing jinja env, if specified, the following parameters will be ignored
    """

    # if not replacement_dict:
    #     return copy.deepcopy(source_obj)

    if isinstance(source_obj, Mapping):
        # dictionary
        dict_class: Type[MutableMapping] = dict
        if not isinstance(dict_class, dict) and isinstance(source_obj, MutableMapping):
            dict_class = source_obj.__class__
        ret: MutableMapping = dict_class()
        for k, v in source_obj.items():
            ret[
                replace_strings_in_obj(k, replacement_dict, jinja_env=jinja_env)
            ] = replace_strings_in_obj(v, replacement_dict, jinja_env=jinja_env)
        return ret
    elif isinstance(source_obj, str):

        # string
        result = process_string_template(
            source_obj, replacement_dict, jinja_env=jinja_env
        )
        if not string_is_templated(source_obj, jinja_env):
            return source_obj

        return result
    elif isinstance(source_obj, Sequence):
        # list (or the like)
        ret_seq: List = []
        for item in source_obj:
            ret_seq.append(
                replace_strings_in_obj(item, replacement_dict, jinja_env=jinja_env)
            )
        return ret_seq
    else:
        # anything else
        return source_obj
