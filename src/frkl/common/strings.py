# -*- coding: utf-8 -*-
import re
import uuid
from typing import Any, Dict, Iterable, List, Mapping, Optional, Union

from frkl.common.defaults import DEFAULT_URL_ABBREVIATIONS_FILE

CAMEL_CALSE_FIRST = re.compile("(.)([A-Z][a-z]+)")
CAMEL_CALSE_ALL = re.compile("([a-z0-9])([A-Z])")

# from django (https://github.com/django/django/blob/master/django/core/validators.py#L74)
# license: https://github.com/django/django/blob/master/LICENSE
# Copyright (c) Django Software Foundation and individual contributors. All rights reserved.
URL_REGEX = re.compile(
    r"^https?://"  # http:// or https://
    r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|"  # domain...
    r"localhost|"  # localhost...
    r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"  # ...or ip
    r"(?::\d+)?"  # optional port
    r"(?:/?|[/?]\S+)$",
    re.IGNORECASE,
)


def from_camel_case(
    text: str,
    sep: str = "_",
    lower: bool = True,
    prefix: Optional[str] = None,
    postfix: Optional[str] = None,
):
    """Convert a string from camel case.

    By default, this converts into snake-case.

    Args:
        text (str): the string
        sep (str): the seperator to use
        lower (bool): whether to convert the result to all lowercase

    Result:
        str: the converted string
    """

    text = CAMEL_CALSE_FIRST.sub(fr"\1{sep}\2", text)
    text = CAMEL_CALSE_ALL.sub(fr"\1{sep}\2", text)
    if lower:
        text = text.lower()

    if prefix:
        text = f"{prefix}{text}"

    if postfix:
        text = f"{text}{postfix}"
    return text


def to_camel_case(text: str) -> str:
    """Convert a string to camel case."""

    # TODO: do that properly, using regex
    text = text.replace("_", " ")
    text = text.replace("-", " ")
    text = text.replace(".", " ")
    return "".join(x for x in text.title() if not x.isspace())


def reindent(
    s: str,
    numSpaces: int,
    keep_current: bool = True,
    line_nrs: Union[bool, int] = False,
):
    """Reindents a string.

    Args:
      s (str): the string
      numSpaces (int): the indent
      keep_current (bool): keep a potential current indention and add to it
      line_nrs (bool, int): whether to display line numbers (if value is int, it signifies the indentation of the line numbers themselves)

    Returns:
        str: the indented string
    """

    tokens: List[str] = s.split("\n")
    if keep_current:
        tokens = [(numSpaces * " ") + line for line in tokens]
    else:
        tokens = [(numSpaces * " ") + line.lstrip() for line in tokens]

    if line_nrs is not False:
        if isinstance(line_nrs, int):
            begin = line_nrs
        else:
            begin = 0

        temp = []
        for index, line in enumerate(tokens):
            temp.append("{:3d}{}".format(index + begin, line))

        tokens = temp

    return "\n".join(tokens)


def expand_git_url(
    url: str, abbrevs: Dict[str, str] = DEFAULT_URL_ABBREVIATIONS_FILE
) -> str:
    """Expands an url using (optionally provided) template values in 'repl_dict'.

    The default abbreviations might be expanded in the future, for now GitHub (gh), GitLab (gl), BitBucket (bb) are supported.

    The format of the url is:

        [service_name]:[user_name]/[repo_name]

    or:

        [service_name]:[user_name]/[repo_name]/path/to/folder/or/file

    or:

        [service_name]:[user_name]::[version_or_branch_or_tag]::/[repo_name]/path/to/folder/or/file

    If no matching service can be found, the input url will be returned.

    """

    template: Optional[str] = None
    abbrev: str
    for key in abbrevs.keys():

        if not url.startswith(key + ":"):
            continue

        template = abbrevs[key]
        abbrev = key
        break

    if template is None:
        return url

    tokens = url[len(abbrev) + 1 :].split("/", maxsplit=3)  # noqa  # type: ignore

    if len(tokens) < 2:
        raise ValueError(
            f"Abbreviated url too short, needs at least user- and repo-name: {url}"
        )

    user = tokens[0]
    repo = tokens[1]

    if "::" in repo:
        if not repo.endswith("::"):
            raise ValueError(
                f"Invalid abbreviation format (if specifying branch, string must end with '::'): {url}"
            )
        repo, branch = repo[0:-2].split("::")
    else:
        branch = "master"

    if len(tokens) >= 3:
        result = template.format(
            user=user, repo=repo, branch=branch, path="/".join(tokens[2:])
        )
    else:
        result = template.format(user=user, repo=repo, branch=branch, path="")

    if result.endswith("/"):
        result = result[0:-1]

    return result


def is_url_or_abbrev(url: str, abbrevs: Dict = DEFAULT_URL_ABBREVIATIONS_FILE) -> bool:
    """Check if the suplied string is an url or url abbreviation.

    Args:
        - *url*: the string to be tested
        - *abbrevs*: an url-abbreviation dictionary

    Returns:
        bool: whether the string is a url or abbreviation
    """

    if not url:
        return False

    for key in abbrevs.keys():
        if url.startswith(f"{key}:"):
            url = expand_git_url(url, abbrevs=abbrevs)
            break

    result = True if URL_REGEX.search(url) else False
    return result


def is_git_repo_url(url) -> bool:
    """Check whether the provided string is a (remote) git repo."""

    # from: https://github.com/jonschlinkert/is-git-url/
    regex = r"([git|ssh|https]|git@[-\w.]+):(\/\/)?(.*?)\.git$"
    match = re.search(regex, url)
    return True if match else False


def find_free_name(
    stem: str,
    current_names: Iterable[str],
    method="count",
    method_args: Mapping[str, Any] = None,
    sep="_",
) -> str:
    """Find a free var (or other name) based on a stem string, based on a list of provided existing names.

    Args:
        stem (str): the base string to use
        current_names (Iterable[str]): currently existing names
        method (str): the method to create new names (allowed: 'count' -- for now)
        method_args (dict): prototing_config for the creation method

    Returns:
        str: a free name
    """

    if method != "count":
        raise NotImplementedError()

    if method_args is None:
        try_no_postfix = True
        start_count = 1
    else:
        try_no_postfix = method_args.get("try_no_postfix", True)
        start_count = method_args.get("start_count", 1)

    if try_no_postfix:
        if stem not in current_names:
            return stem

    i = start_count

    # new_name = None
    while True:
        new_name = f"{stem}{sep}{i}"
        if new_name in current_names:
            i = i + 1
            continue
        break
    return new_name


def _generate_valid_identifier_seq(text, sep=None, allow_leading_underscore=False):
    """Helper generator for 'generate_valid_identifier."""

    itr = iter(text)
    # pull characters until we get a legal one for first in identifer
    for ch in itr:
        if (allow_leading_underscore and ch == "_") or ch.isalpha():
            yield ch
            break
    # pull remaining characters and yield legal ones for identifier
    for ch in itr:
        if ch == "_" or ch.isalpha() or ch.isdigit():
            yield ch
        elif sep:
            yield sep


def generate_valid_identifier(
    name: Any = None,
    first_character_uppercase=False,
    prefix: Optional[str] = None,
    length_without_prefix: int = -1,
    sep: Optional[str] = None,
    allow_leading_underscore: bool = False,
):
    """Generate a valid Python identifier, either randomly, or from a string, or a combination of both.

    Args:
        name: an optional string or object (will be converted with 'str' method), to be used as base to generate the id, if not provided, a uuid string will be used
        first_character_uppercase (bool): whether the first character of the generated id should be uppercase
        prefix (Optional[str]): a prefix for the provided 'name'
        length_without_prefix (int): how long the generated id should be
        sep (Optional[str]): separator char, defaults to None
        allow_leading_underscore (bool): whether the resulting id can have a "_" as first character.
    """

    if name is None:
        name = str(uuid.uuid4())
    elif not isinstance(name, str):
        name = str(name)

    if prefix:
        name = f"{prefix}{name}"

    id = "".join(
        _generate_valid_identifier_seq(
            name, sep=sep, allow_leading_underscore=allow_leading_underscore
        )
    )
    if first_character_uppercase:
        id = id.capitalize()

    if length_without_prefix > 0:
        if prefix:
            id = id[0 : length_without_prefix + len(prefix)]  # noqa
        else:
            id = id[0:length_without_prefix]

    return id
