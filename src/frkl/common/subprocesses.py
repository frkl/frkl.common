# -*- coding: utf-8 -*-
import asyncio
import collections
import logging
import os
import re
from asyncio.subprocess import Process
from typing import Any, Iterable, List, Mapping, Optional, Union

log = logging.getLogger("frkl")


class SubprocessException(Exception):
    def __init__(self, subprocess: "AsyncSubprocess") -> None:

        cmd = subprocess.current_state["command"]
        ret_code = subprocess.current_state["returncode"]
        stdout = subprocess.current_state["stdout"]
        if stdout is None:
            stdout = ""
        stderr = subprocess.current_state["stderr"]
        if stderr is None:
            stderr = ""
        self._msg = f"Error executing command '{cmd}': {ret_code} - {stdout}/{stderr}"
        self._subprocess: AsyncSubprocess = subprocess
        super(SubprocessException, self).__init__(self._msg)


class AsyncSubprocess(object):
    def __init__(
        self,
        command: str,
        *args,
        working_dir: str = None,
        expected_ret_code=0,
        extra_path: Union[None, str, Iterable[str]] = None,
        **env_vars,
    ) -> None:

        self._command = command
        self._args = args
        self._proc: Optional[Process] = None
        if working_dir is None:
            working_dir = os.getcwd()
        self._working_dir = os.path.realpath(working_dir)
        self._env_vars = env_vars
        self._expected_ret_code = expected_ret_code
        self._raise_exception = True

        _path: List[str] = os.environ.get("PATH", os.defpath).split(os.pathsep)

        if extra_path:
            if isinstance(extra_path, str):
                _extra_path: List[str] = extra_path.split(os.pathsep)
            elif isinstance(extra_path, collections.abc.Iterable):
                _extra_path = list(extra_path)
            else:
                raise TypeError(
                    f"Can't execute external command, invalid path type '{type(extra_path)}': {extra_path}"
                )

            _path = _extra_path + _path

        self._path: List[str] = _path

        self._return_code: Optional[int] = None
        self._stdout: Optional[str] = None
        self._stderr: Optional[str] = None

        # self._stdout_lines: Optional[List[str]] = None
        # self._stderr_lines: Optional[List[str]] = None
        self._success: Optional[bool] = None

    async def run(self, wait=True, expected=None, raise_exception=True) -> Process:

        self._raise_exception = raise_exception
        if expected is not None:
            if not isinstance(expected, int):
                raise TypeError("'expected' value needs to be integer.")
            self._expected_ret_code = expected

        env = dict(os.environ)  # make a copy of the environment
        env.update(self._env_vars)

        lp_key = "LD_LIBRARY_PATH"  # for Linux and *BSD.
        lp_orig = env.get(lp_key + "_ORIG")  # pyinstaller >= 20160820 has this
        xdb_key = "XDG_DATA_DIRS"
        xdb_orig = env.get(xdb_key + "_ORIG")

        if lp_orig:
            env[lp_key] = lp_orig
        else:
            lp = env.get(lp_key, None)
            if lp is not None and re.search(r"/_MEI......", lp):
                env.pop(lp_key)
        if xdb_orig:
            env[xdb_key] = xdb_orig

        env["PATH"] = os.pathsep.join(self._path)

        self._proc = await asyncio.create_subprocess_exec(
            self._command,
            *self._args,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
            env=env,
            cwd=self._working_dir,
        )

        if wait:
            await self.wait()

        return self._proc

    @property
    def current_state(self) -> Mapping[str, Any]:

        return {
            "command": self._command,
            "stdout": self._stdout,
            "stderr": self._stderr,
            "returncode": self._return_code,
            "is_running": self._proc is not None,
        }

    @property
    async def returncode(self) -> int:

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._return_code  # type: ignore

    @property
    async def stdout_lines(self) -> List[str]:

        stdout = await self.stdout
        if stdout == "":
            return []
        else:
            return stdout.split("\n")

    @property
    async def stderr_lines(self) -> List[str]:

        stderr = await self.stderr
        if stderr == "":
            return []
        else:
            return stderr.split("\n")

    @property
    async def stdout(self) -> str:

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._stdout  # type: ignore

    @property
    async def stderr(self) -> str:

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._stderr  # type: ignore

    @property
    async def success(self) -> bool:

        if self._proc is None:
            raise Exception("No process started yet.")

        await self.wait()
        return self._success  # type: ignore

    async def wait(self) -> bool:

        if self._proc is None:
            raise Exception("No running process to wait on.")

        if self._success is not None:
            return self._success

        (stdout, stderr) = await self._proc.communicate()

        self._stdout = stdout.decode()
        self._stderr = stderr.decode()

        # self._stdout_lines = stdout.decode().split("\n")
        # self._stderr_lines = stderr.decode().split("\n")

        self._return_code = self._proc.returncode

        self._success = self._return_code == self._expected_ret_code

        if not self._success and self._raise_exception:
            raise SubprocessException(subprocess=self)

        return self._success

    async def kill(self, wait=True) -> None:

        if self._proc is None:
            raise Exception("No process started yet.")

        try:
            self._proc.kill()
        except (ProcessLookupError):
            pass
        if wait:
            pass
            # await self._proc.wait()

    async def __aenter__(self) -> "AsyncSubprocess":

        await self.run(wait=False, raise_exception=True)
        # self.run(wait=False)
        return self

    async def __aexit__(self, *args) -> None:

        try:
            await self.kill()
        except (Exception) as e:
            import traceback

            traceback.print_exc()
            print(e)

    def __repr__(self) -> str:

        started = self._proc is not None
        finished = self._success is not None
        if finished:
            status = "finished"
            msg = f", ret_code={self._return_code}"
        else:
            status = "started" if started else "no process"
            msg = ""
        return f"[AsyncSubprocess: command={self._command}, args={self._args} status={status}{msg}]"


class GitProcess(AsyncSubprocess):
    def __init__(
        self,
        git_command: str,
        *args,
        working_dir: str = None,
        expected_ret_code=0,
        **env_vars,
    ):

        git_args = [git_command]
        git_args.extend(args)
        super().__init__(
            "git",
            *git_args,
            working_dir=working_dir,
            expected_ret_code=expected_ret_code,
            **env_vars,
        )
