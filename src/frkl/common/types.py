# -*- coding: utf-8 -*-
import collections
import importlib
import inspect
import logging
import pkgutil
from types import ModuleType
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Type,
    Union,
)

from frkl.common.defaults import DEFAULT_KEY_FORMATS
from frkl.common.environment import is_debug
from frkl.common.exceptions import FrklException
from frkl.common.strings import from_camel_case


def isinstance_or_subclass(obj: Any, base_class: Type) -> bool:

    return isinstance(obj, base_class) or issubclass(obj.__class__, base_class)


log = logging.getLogger("frkl")


def get_all_subclasses(cls: Type) -> List[Type]:
    """Find all (loaded) subclasses of a given base-class.

    Args:
      cls: the base class

    Returns:
      a list of all subclasses
    """
    all_subclasses = []

    # import pp
    # print(prototing_class)
    # pp(type(prototing_class))
    # pp(prototing_class.__dict__)

    for subclass in cls.__subclasses__():
        all_subclasses.append(subclass)
        all_subclasses.extend(get_all_subclasses(subclass))

    return all_subclasses


def get_class_from_string(class_name: str) -> Type:
    """Return the class type object of the specified class name.

    Args:
      class_name: the class path string

    Returns:
      str: the Type object of the requested class
    """

    if "." in class_name:
        module_name, _, class_name = class_name.rpartition(".")
        return get_class(module_name, class_name)
    else:
        raise ValueError(
            f"No module name provided when trying to get class '{class_name}'"
        )


def get_class(module_name: str, class_name: str) -> Type:
    """Return a class type object of the specified module and class names.

    Raises an `ImportError` if the module can't be loaded, and an `AttributeError` if the class can't be found.

    Args:
      module_name (str): the module name
      class_name (str): the class name

    Returns:
        - the Type object of the requested class
    """

    if not module_name:
        raise ValueError(
            f"No module name provided when trying to get class '{class_name}'"
        )
    m = importlib.import_module(module_name)
    c = getattr(m, class_name)

    return c


def get_type_alias_string(
    ty: Union[Type, str], include_module_path=True, alias_type="underscore"
) -> str:
    """Return a string alias for the provided type.

    Check this methods source code for details on how the alias string is generated.

    Args:
        ty (Union[Type, str]): the type
        include_module_path (bool): whether to include the module path in the returned alias
        alias_type (str): how to assemble the alias string, only 'underscore' supported for now

    Returns:
        str: the alias
    """

    if alias_type != "underscore":
        raise NotImplementedError()

    if isinstance(ty, type):
        type_name = ty.__name__
    else:
        type_name = ty.split(".")[-1]

    alias = from_camel_case(type_name, sep="_")

    if include_module_path:
        if isinstance(ty, str):
            if "." in ty:
                module_path = ty.split(".")[0:-1]
                module_path.append(alias)
                alias = ".".join(module_path)
        else:
            alias = f"{ty.__module__}.{alias}"

    return alias


def create_type_alias_map(
    type_obj: Type,
    key_formats: Optional[Iterable[str]] = None,
    remove_postfixes: Optional[Iterable[str]] = None,
):
    """Create a map of aliases for a a Class.

    This is mostly used to be able to lookup classes by using 'plugin_names'
    (aka: easy to remember names for users).

    Args:
        type_obj (Type): the class
        key_formats (Optional[Iterable[str): one or several format names (available: 'class_name', 'dash', 'underscore'), if none provided all will be used
        remove_postfixes (Optional[Iterable[str]]): a list of strings/characters to remove from the aliases if found (case ignore)
    """

    if not key_formats:
        key_formats = ["class_name", "dash", "underscore"]

    result = {}
    type_name = type_obj.__name__

    aliases = set()
    if "class_name" in key_formats:
        aliases.add(type_name)
    if "dash" in key_formats:
        tsc_id_string = from_camel_case(type_name, sep="-")
        aliases.add(tsc_id_string)
    if "underscore" in key_formats:
        tsc_id_string = from_camel_case(type_name, sep="_")
        tsc_id_string_dash = tsc_id_string.replace("-", "_")
        aliases.add(tsc_id_string_dash)

    for alias in aliases:

        key = alias
        if remove_postfixes:
            if isinstance(remove_postfixes, str):
                remove_postfixes = [remove_postfixes]

            for pf in remove_postfixes:
                try:
                    if alias.lower().endswith(pf.lower()):
                        temp = alias[0 : -len(pf)]  # noqa
                        if temp:

                            if temp[-1] in ["_", "-"]:
                                temp = temp[0:-1]
                                if temp:
                                    key = temp
                            else:
                                key = temp

                except (Exception) as e:
                    log.debug(f"Could not remove postfix: {e}", exc_info=True)

        result[key] = type_obj

    return result


def add_type_aliases(
    type_map: MutableMapping[str, Type],
    type_obj: Type,
    key_formats: Iterable[str] = DEFAULT_KEY_FORMATS,
    remove_postfixes: Optional[Iterable[str]] = None,
) -> None:
    """Add aliases of a specified class to a provided type map.

    This is used to build up a dictionary of available classes and their aliases (basically a plugin registry).

    Check 'create_type_alias_map' for argument specs.
    """

    aliases = create_type_alias_map(
        type_obj=type_obj, key_formats=key_formats, remove_postfixes=remove_postfixes
    )

    for k, v in aliases.items():
        if k in type_map.keys() and v != type_map[k]:
            raise FrklException(
                msg=f"Can't add type alias '{k}' for class '{v}'.",
                reason=f"Alias already used for class '{type_map[k]}'.",
            )

        type_map[k] = v


def load_modules(*modules: Union[None, str, Iterable[str]]) -> List[ModuleType]:
    """Load all specified modules by (string-)name.

    If an item ends with '.*', all child modules will be loaded. No other
    wildcards/wildcard positions are supported for now.

    Args:
        *modules: a list of modules

    Returns:
        List: a list of module objects that were loaded
    """

    result: List[ModuleType] = []
    if not modules:
        return result

    for mod in modules:
        if not mod:
            continue
        if isinstance(mod, str):
            if "*" in mod:
                if not mod.endswith(".*"):
                    raise FrklException(
                        msg=f"Can't import module string '{mod}'.",
                        reason="String must either not contain '*', or end with '.*'.",
                    )

                base = ".".join(mod.split(".")[0:-1])
                base_mod = importlib.import_module(base)
                result.append(base_mod)

                # prefix = base_mod.__name__ + "."
                prefix = base + "."
                for p in pkgutil.iter_modules(
                    base_mod.__path__, prefix  # type: ignore
                ):
                    m = __import__(p[1], fromlist="dummy")
                    result.append(m)

                # special handling when the package is bundled with PyInstaller 3.5
                # See https://github.com/pyinstaller/pyinstaller/issues/1905#issuecomment-445787510
                toc: Any = set()
                for importer in pkgutil.iter_importers(
                    base_mod.__name__.partition(".")[0]
                ):
                    if hasattr(importer, "toc"):
                        toc |= importer.toc  # type: ignore
                for name in toc:
                    if name.startswith(prefix):
                        m = __import__(name, fromlist="dummy")
                        result.append(m)

            else:
                m = importlib.import_module(mod)
                result.append(m)
        elif isinstance(mod, collections.Iterable):
            ms = load_modules(*mod)
            result.extend(ms)
        else:
            raise TypeError(f"Invalid module type: {type(mod)}")

    return result


def find_sub_classes(
    base_class: Type,
    preload_modules: Union[str, Iterable[str], None] = None,
    key_formats: Iterable[str] = DEFAULT_KEY_FORMATS,
    remove_postfixes: Iterable[str] = None,
) -> Mapping[str, Type]:
    """Find all (non-abstract) classes that extend the provided base class.

    Args:
        base_class (Type): the base class
        preload_modules (Union[str, Iterable[str]]): an optional list of modules to preload before starting the search
        key_formats (Optional[Iterable[str]]): a list of key formats to be used in alias creation
        remove_postfixes (Optional[Iterable[str]]): a list of postfixes to be removed from generated aliases

    Returns:
        Mapping: a dict of string aliases and the classes they refer to

    """

    load_modules(preload_modules)

    type_subclasses = get_all_subclasses(base_class)
    result: Dict[str, Type] = {}

    for tsc in type_subclasses:
        if inspect.isabstract(tsc):
            if is_debug():
                log.warning(f"not using subclass '{tsc}': class is abstract")
            else:
                log.debug(f"not using subclass '{tsc}': class is abstract")
            continue
        add_type_aliases(
            result,
            type_obj=tsc,
            key_formats=key_formats,
            remove_postfixes=remove_postfixes,
        )

    return result


class Singleton(type):
    """A 'singleton' metaclass.

    Adapted from: https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python/6760821
    """

    _instances: Dict[Type, Type] = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)

        return cls._instances[cls]
