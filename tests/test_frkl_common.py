#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_common` package."""

import pytest  # noqa

import frkl.common


def test_assert():

    assert frkl.common.get_version() is not None
